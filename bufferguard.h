/*!
 * \file
 * bufferguard.h
 *
 * \author
 * Aleš Podskalský (<a href="mailto:podskale@fel.cvut.cz">podksale@fel.cvut.cz</a>)
 *
 * \brief
 * KOmponenta pro zobrazeni prubehu prehravani.
 */

#ifndef BUFFERGUARD_H
#define BUFFERGUARD_H

#include <QObject>

/*!
 * \brief
 * Kontroluje zda buffer má dostatečný náskok přd přehráváním.
 */
class BufferGuard : public QObject
{
    Q_OBJECT
    public:
        BufferGuard();
        virtual ~BufferGuard();

    public:
        qint64 getTotalTime() const;
        qint64 getBufferSize() const;
        int    getPreloadTime() const;

        bool   isReady() const;
        qint64 alignValue(qint64 value) const;

    public slots:
        void setTotalTime(qint64 time);
        void setBufferSize(qint64 size);
        void setPreloadTime(int time);

        void timeTick(qint64 value);
        void bufferTick(qint64 value);

    signals:
        void preloadExceeded();

    private:
        int recoutTimeRate();
        bool isValidTime(qint64 value) const;
    private:
        int    m_preloadTime;
        qint64 m_totalTime;
        qint64 m_bufferSize;
        qint64 m_timeRate;

        qint64 m_timeValue;
        qint64 m_bufferValue;

};

#endif // BUFFERGUARD_H
