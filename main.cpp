#include <QtGui>
#include <QtGui/QApplication>
#include <QTextCodec>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    Q_INIT_RESOURCE(resources);
    QApplication app(argc, argv);


    app.setApplicationName("CT KuK");
    app.setApplicationVersion("0.2");
    app.setOrganizationName("Y36API");
    app.setQuitOnLastWindowClosed(true);


	QTextCodec *locale = QTextCodec::codecForLocale();
	QTextCodec::setCodecForCStrings(locale);
	QTextCodec::setCodecForTr(locale);
    
    QTranslator _translate;
    _translate.load(":languages/CTKuK_cs_CZ.qm");

    app.installTranslator(&_translate);

    MainWindow w;
    w.show();

    return app.exec();
}
