#ifndef _SHOW_INFO_H_
#define _SHOW_INFO_H_

#include <QVector>
#include <QString>

#include "VideoInfo.h"

struct ShowInfo
{
	QString title;
	QString description;
	QString webPage;
	QVector<VideoInfo *> videos;
	QVector<VideoInfo *> bonusVideos;
	//
	~ShowInfo();
};

#endif	// _SHOW_INFO_H_
