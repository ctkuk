#include "WebEngineError.h"

#include <QObject>
#include <QVector>

WebEngineError::WebEngineError(int code)
{
	m_code = code;
	getErrDesc(code);
}

void
WebEngineError::getErrDesc(int code)
{
	// tr() musim dat s QObject::, protoze trida neni Qt trida (= potomek QObject)
	static QVector<QString> errTable;
	if(errTable.size() == 0)
	{
		errTable.push_back(QObject::tr("Ok."));	// ERR_NO
        errTable.push_back(QObject::tr("Execution error!"));	// ERR_REQUEST
        errTable.push_back(QObject::tr("Loading parser error!"));	// ERR_PARSER
        errTable.push_back(QObject::tr("Parser error!"));	// ERR_PARSING
	}
	if(code >= errTable.size())
	{
        m_desc = QObject::tr("Unexpected error!");
	}
	m_desc = errTable[code];
}
