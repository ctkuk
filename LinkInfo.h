#ifndef _LINK_INFO_H_
#define _LINK_INFO_H_

#include <QString>

struct LinkInfo
{
	QString id;
	QString title;
};

#endif	// _LINK_INFO_H_
