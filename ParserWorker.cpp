#include "ParserWorker.h"

#include <QTextStream>
#include <QFile>
#include <QXmlQuery>

ParserWorker::ParserWorker()
{
    m_stop = false;
}

ParserWorker::~ParserWorker()
{
    //
}

void
ParserWorker::addTask(QPair<QString, QByteArray *> *task)
{
    m_mutex.lock();
    m_taskList.push_back(task);
    m_mutex.unlock();
}

void
ParserWorker::cleanTasks()
{
    m_mutex.lock();
    m_taskList.clear();
    m_mutex.unlock();
}

void
ParserWorker::run()
{
    QPair<QString, QByteArray *> *task;
    int err;
    QString result;
    QString tmp;
    VideoInfo *video;
    //
    while(1)
    {
        m_mutex.lock();
        //
        // Stop?
        if(m_stop) break;
        //
        // Get new Task
        if(m_taskList.size() > 0)
        {
            task = m_taskList.first();
            m_taskList.pop_front();
            //
            m_mutex.unlock();   // UNLOCK!
        }
        else
        {
            m_mutex.unlock();   // UNLOCK!
            //
            usleep(100000); // 100ms
            continue;
        }
        //
        // Parse
        if((err = parse(task->first, *(task->second), &result)) != ERR_NO)
        {
            emit error(new WebEngineError(err));
            //
            delete task->second;
            delete task;
            //
            continue;
        }
        //
        // Get data
        QTextStream stream(&result, QIODevice::ReadOnly);
        while(1)
        {
            tmp = stream.readLine();
            if(tmp.isNull()) break;
            //
            tmp = tmp.trimmed();
            if(tmp.length() > 0)
            {
                video = new VideoInfo;
                video->id = tmp;
                video->date = QDate::fromString(stream.readLine().trimmed(), "d. M. yyyy");
                video->img = stream.readLine().trimmed();
                video->desc = stream.readLine().trimmed();
                if(video->desc.length() == 0) video->desc = "-- bez popisu --";
                video->watched = stream.readLine().trimmed().toInt();
                emit showVideoReady(video);
            }
        }
        //
        // Clean up
        delete task->second;
        delete task;
    }
    //
    m_mutex.lock();
    for(int i = 0, im = m_taskList.size(); i < im; ++i)
        delete m_taskList[i];
    //
    m_taskList.clear();
    m_mutex.unlock();
}

void
ParserWorker::stop()
{
    m_mutex.lock();
    m_stop = true;
    m_mutex.unlock();
}

int
ParserWorker::parse(const QString &parserPath, const QByteArray &data, QString *result)
{
    if(result == NULL)
    {
        return ERR_PARSING;
    }
    //
    QFile parser(parserPath);
    parser.open(QIODevice::ReadOnly);
    if(!parser.isOpen())
    {
        return ERR_PARSER;
    }
    QByteArray get_videos = parser.readAll();
    parser.close();
    //
    QXmlQuery query(QXmlQuery::XQuery10);
    QByteArray _data = data;
    if(query.setFocus(fromUtf8toLocal8Bit(_data)))
    {
        query.setQuery(get_videos);
        if(query.isValid())
        {
            query.evaluateTo(result);
        }
        else
        {
            return ERR_PARSER;
        }
    }
    else
    {
        return ERR_PARSING;
    }
    return ERR_NO;
}

QByteArray &
ParserWorker::fromUtf8toLocal8Bit(QByteArray &arr) const
{
    arr = QString::fromUtf8(arr.data()).toLocal8Bit();
    return arr;
}
