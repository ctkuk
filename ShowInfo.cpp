#include "ShowInfo.h"

ShowInfo::~ShowInfo()
{
	for(int i = 0, im = videos.size(); i < im; ++i)
		delete videos[i];
	//
	for(int i = 0, im = bonusVideos.size(); i < im; ++i)
		delete bonusVideos[i];
	//
	videos.clear();
	bonusVideos.clear();
}
