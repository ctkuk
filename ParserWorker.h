#ifndef PARSERWORKER_H
#define PARSERWORKER_H

#include <QThread>
#include <QMutex>
#include <QString>
#include <QVector>
#include <QPair>
#include <QByteArray>

#include "WebEngineError.h"
#include "VideoInfo.h"

class ParserWorker : public QThread
{
    Q_OBJECT

    signals:
        void showVideoReady(VideoInfo *video);
        void error(WebEngineError *error);

    private:
        QVector<QPair<QString, QByteArray *> *> m_taskList;
        QMutex m_mutex;
        bool m_stop;

    public:
        ParserWorker();
        ~ParserWorker();

        void addTask(QPair<QString, QByteArray *> *task);
        void cleanTasks();
        void run();
        void stop();
        int parse(const QString &parserPath, const QByteArray &data, QString *result);
        QByteArray & fromUtf8toLocal8Bit(QByteArray &arr) const;
};

#endif // PARSERWORKER_H
