#ifndef _CONFIG_H_
#define _CONFIG_H_

#define CONFIG_NETWORK 1

#if defined(WIN32) || defined(WIN64)
    #define HAVE_WINSOCK2_H 1
    #define strncasecmp strnicmp
    #define strcasecmp stricmp
    #define snprintf _snprintf
    #define strtoll strtol
    #define atoll atol
#endif /* Def WIN32 or Def WIN64 */

#endif	// _CONFIG_H_
