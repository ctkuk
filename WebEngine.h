/*!
 * \file
 * WebEngine.h
 * 
 * \author
 * Martin Ovesný (<a href="mailto:ovesnmar@fel.cvut.cz">ovesnmar@fel.cvut.cz</a>)
 *
 * \brief
 * Engine pro získávání informací z webu iVysílání.cz
 */
#ifndef _WEB_ENGINE_H_
#define _WEB_ENGINE_H_

#include <QObject>
#include <QVector>
#include <QString>
#include <QDate>
#include <QByteArray>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>

#include "WebEngineError.h"
#include "LinkInfo.h"
#include "VideoInfo.h"
#include "ShowInfo.h"
#include "ParserWorker.h"

/*!
 * \brief
 * Třída pracující se servrem iVysílání.cz pomocí HTTP.
 * Lze získat seznamy pořadů, informace o jednotlivých pořadech,
 * či linky pro streamování jednotlivých videí.
 */
class WebEngine : public QObject
{
	Q_OBJECT
	
	private:
		QObject *m_parent;
		QNetworkAccessManager *m_net;
		QNetworkRequest *m_request;
		QNetworkReply *m_reply;
        QNetworkAccessManager *m_netVideo;
        QNetworkRequest *m_requestVideo;
        QNetworkReply *m_replyVideo;
		bool m_working;
		QString m_requestedShowId;
        QNetworkReply *m_threadReply;
        ParserWorker m_parser;
		
	private slots:
		void slotNewestVideosReady(QNetworkReply *reply);
		void slotMostWatchedVideosReady(QNetworkReply *reply);
		void slotAlphabetReady(QNetworkReply *reply);
		void slotGenresReady(QNetworkReply *reply);
		void slotShowsByAlphaReady(QNetworkReply *reply);
		void slotShowsByGenreReady(QNetworkReply *reply);
		void slotVideosByDateReady(QNetworkReply *reply);
		void slotShowVideosReady(QNetworkReply *reply);
		void slotShowBonusVideosReady(QNetworkReply *reply);
		void slotVideoLinkReady(QNetworkReply *reply);
		void slotRemoteFileReady(QNetworkReply *reply);
		void slotError(QNetworkReply::NetworkError errCode);
        void emitError(WebEngineError *error);
        void emitShowVideoReady(VideoInfo *video);
		
	signals:
		void newestVideosReady(QVector<VideoInfo *> *videos);
		void mostWatchedVideosReady(QVector<VideoInfo *> *videos);
		void alphabetReady(QVector<LinkInfo *> *genres);
		void genresReady(QVector<LinkInfo *> *genres);
		void showsByAlphaReady(QVector<LinkInfo *> *shows);
		void showsByGenreReady(QVector<LinkInfo *> *shows);
		void videosByDateReady(QVector<LinkInfo *> *videos);
		void showAllVideosReady();
        void showVideoReady(VideoInfo *video);
		void showBonusVideosReady(QVector<VideoInfo *> *videos);
		void videoLinkReady(QString *url);
		void remoteFileReady(QByteArray *data);
        void error(WebEngineError *err);

	protected:
#ifdef HTMLTIDY_LOCAL
		QByteArray & htmlTidy(QByteArray &arr) const;	// HTML Tidy Project: http://tidy.sourceforge.net/
#endif	// HTMLTIDY_LOCAL
		QString      htmlTidyViaWeb(const QString &url) const;	// HTML Tidy Project: http://tidy.sourceforge.net/
        QByteArray & fromUtf8toLocal8Bit(QByteArray &arr) const;
		void         doRequest(const QString &url, bool tidyHtml = true);
        void         doVideoRequest(const QString &url, bool tidyHtml = true);
		int          parse(const QString &parserPath, const QByteArray &data, QString *result);
		
	public:
		/*!
		 * \brief
		 * Součást HTTP hlavičky, díky které se klient
		 * tváří jako obyčejný webový prohlížeč.
		 */
		static QByteArray userAgent;
		
		/*!
		 * \brief
		 * Adresa archivu České televize - iVysílaní.cz
		 *
		 * Lze použít pro filtrování pořadů:
		 * - Datum: http://www.ceskatelevize.cz/ivysilani/20.02.2010/
		 * - Abeceda: http://www.ceskatelevize.cz/ivysilani/a/
		 * - Žánr: http://www.ceskatelevize.cz/ivysilani/dokumenty/
		 */
		static QString iVysilaniLink;
		
		/*!
		 * \brief
		 * Adresa seznamu nejnovějších pořadů.
		 */
		static QString theNewestLink;
		
		/*!
		 * \brief
		 * Adresa seznamu nejsledovanějších pořadů.
		 */
		static QString theMostWatchedLink;
		
		/*!
		 * \brief
		 * Adresa seznamu žánrů a znaků abecedy, mezi nimiž lze hledat pořady.
		 */
		static QString alphabeticalySortedLink;
		
		/*!
		 * \brief
		 * Konstruktor.
		 *
		 * \param parent
		 * Ukazatel na objekt, jež volá konstruktor této třídy - slouží k automatickému uvolňování
		 * paměti pomocí hierarchie Qt tříd. Parametr není potřeba vyplňovat - paměť bude uvolněna tak, jako tak.
		 */
		WebEngine(QObject *parent = NULL);
		
		/*!
		 * \brief
		 * Destruktor.
		 */
		~WebEngine();
		
		void getNewestVideos();
		void getMostWatchedVideos();
		void getAlphabet();
		void getGenres();
		void getShowsByAlpha(const QString &c);
		void getShowsByGenre(const QString &genre);
		void getVideosByDate(const QDate &date);
		void getShowVideos(const QString &id, int page = 1);
		void getShowBonusVideos(const QString &id);
		void getVideoLink(const QString &id);
		void getRemoteFile(const QString &url);
		bool isWorking() const;
};

#endif	// _WEB_ENGINE_H_
