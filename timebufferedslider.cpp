#include "timebufferedslider.h"

#include <QPainter>

/*======================== Contructors / Destructor =============================*/

TimeBufferedSlider::TimeBufferedSlider( QWidget* parent ): Slider( parent )
{
    m_sliding = false;
    m_outside = false;
    m_prevValue = 0;
    m_bufferValue = 0;
    m_pMediaObject = 0;

    setRange( 0, 100 );
}

TimeBufferedSlider::~TimeBufferedSlider()
{


}

/*======================== Public slots =============================*/

void TimeBufferedSlider::setBufferValue( int value)
{
    m_bufferValue = value;
    //
    this->repaint();
}

void TimeBufferedSlider::setMediaObject ( Phonon::MediaObject * media )
{
    m_pMediaObject = media;
    m_pMediaObject->setTickInterval(1000);
    setMinimum(0);


    connect(media,SIGNAL(hasVideoChanged(bool)),this,SLOT(mediaVideoChanged(bool)));

}

void TimeBufferedSlider::mediaVideoChanged(bool hasVideo)
{
    setEnabled(hasVideo);
    if (hasVideo)
    {
        connect(m_pMediaObject,SIGNAL(tick( qint64)),this,SLOT(setValue( qint64 )));
    }
    else
    {
        disconnect(m_pMediaObject,SIGNAL(tick( qint64)),this,SLOT(setValue( qint64 )));
    }
}


/*======================== Public functions =============================*/


int TimeBufferedSlider::valueBuffer() const
{
    return m_bufferValue;
}


int TimeBufferedSlider::maximumBuffer() const
{
    return m_bufferMaximum;
}

int TimeBufferedSlider::minimumBuffer() const
{
    return m_bufferMinimum;
}

void TimeBufferedSlider::setMaximumBuffer(int max)
{
    m_bufferMaximum = max;
}

void TimeBufferedSlider::setMinimumBuffer(int min)
{
    m_bufferMinimum = min;

}

void TimeBufferedSlider::setRangeBuffer(int min, int max)
{
    setMinimumBuffer(min);
    setMaximumBuffer(max);
}

void TimeBufferedSlider::paintCustomSlider( QPainter *p, int x, int y, int width, int height)
{
    p->setPen(Qt::white);
    QLinearGradient linearGrad(QPointF(width, 0), QPointF(width, height));
    linearGrad.setColorAt(0, Qt::gray);
    linearGrad.setColorAt(isEnabled() ? 1 : 0.7, Qt::white);
    p->setBrush(linearGrad);

    p->drawRoundedRect(0,height/2-5,width-1,10,5,5);


    linearGrad.setColorAt(0, Qt::red);
    linearGrad.setColorAt(.7, Qt::white);
    p->setBrush(linearGrad);

    int _bufferValue = m_bufferMinimum;
    qint64 _xpos = -6;

    if (isEnabled())
    {
		qint64 diff = maximum() - minimum();
        _xpos = ((diff == 0) ? 0 : (width*value()/diff))-6;
        _bufferValue = m_bufferValue;
    }
    if (_xpos > width-13)
        _xpos = width-13;
    if (_xpos < 0)
        _xpos = 0;

    if (_bufferValue > 0)
    {
		qint64 diff = m_bufferMaximum - m_bufferMinimum;
		p->drawRoundedRect(2,height/2-3,((diff == 0) ? 0 : ((width-1)*_bufferValue/diff))-5,6,2,2);
    }

    QRadialGradient radialGrad(QPointF(_xpos+3,height/2-6), 12);
    radialGrad.setColorAt(0, Qt::white);
    radialGrad.setColorAt(1, isEnabled() ? Qt::black: Qt::gray);
    p->setBrush(radialGrad);


    p->drawEllipse(_xpos,height/2-6,12,12);
}
