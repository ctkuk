<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="cs_CZ">
<defaultcodec>UTF-8</defaultcodec>
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="dialogs/about/aboutdialog.ui" line="17"/>
        <source>About CT KuK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/about/aboutdialog.ui" line="33"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/about/aboutdialog.ui" line="65"/>
        <source>Cílem projektu ČT KuK je vytvoření klienta pro internetový portál České televize ivysilani.cz, zprostředkovávajícího televizní vysílání pomocí video streamů. Program bude schopen procházet seznam videí, nacházející se na tomto webu, a zároveň tato videa sám přehrávat, popřípadě uložit pro pozdější shlédnutí offline.
Aplikace bude schopna přetáčení v již stažené části videa a pozastavení přehrávání (pauza). Video se bude přehrávat v okně přehrávače s možností zobrazit jej na celou obrazovku.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialogs/about/aboutdialog.ui" line="89"/>
        <source>ČT KuK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.cpp" line="18"/>
        <source>CT KuK</source>
        <translation type="unfinished">ČT KuK</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="55"/>
        <source>&amp;File</source>
        <translation type="unfinished">&amp;Soubor</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="57"/>
        <source>&amp;Save buffer..</source>
        <translation type="unfinished">U&amp;ložit pořad..</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="59"/>
        <source>E&amp;xit</source>
        <translation type="unfinished">U&amp;končit</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="62"/>
        <source>&amp;Playing</source>
        <translation type="unfinished">&amp;Přehrávání</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="63"/>
        <source>&amp;Play/Pause</source>
        <translation type="unfinished">P&amp;řehrát/Pozastavit</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="64"/>
        <source>&amp;Stop</source>
        <translation type="unfinished">&amp;Zastavit</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="66"/>
        <source>&amp;View</source>
        <translation type="unfinished">&amp;Pohled</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="68"/>
        <source>&amp;Full view</source>
        <translation type="unfinished">P&amp;lné zobrazení</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="72"/>
        <source>&amp;Player view</source>
        <translation type="unfinished">&amp;Zobrazení přehrávače</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="75"/>
        <source>&amp;Minimal view</source>
        <translation type="unfinished">&amp;Minimální zobrazení</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="83"/>
        <source>&amp;Help</source>
        <translation type="unfinished">&amp;Nápověda</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="84"/>
        <source>&amp;About</source>
        <translation type="unfinished">&amp;O programu</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="90"/>
        <source>&amp;Check for updates</source>
        <translation type="unfinished">Z&amp;kontrolovat aktualizace</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="210"/>
        <source>Programme: </source>
        <translation type="unfinished">Pořad:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="214"/>
        <source>Episode: </source>
        <translation type="unfinished">Díly:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="234"/>
        <source>Loading list of the programmes...</source>
        <translation type="unfinished">Seznam pořadů...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="301"/>
        <source>Loading video...</source>
        <translation type="unfinished">Nahrávám video...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="414"/>
        <source>Error!</source>
        <translation type="unfinished">Chyba!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="419"/>
        <source>Paused</source>
        <translation type="unfinished">Pozastaveno</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="429"/>
        <source>Stopped</source>
        <translation type="unfinished">Zastaveno</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="650"/>
        <source>Loading buffer...</source>
        <translation type="unfinished">Načítám video...</translation>
    </message>
    <message>
        <source>Video is ready for playing.</source>
        <translation type="obsolete">Video je připraveno pro přehrátí.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="442"/>
        <source>Playing...</source>
        <translation type="unfinished">Přehrávání...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="521"/>
        <location filename="mainwindow.cpp" line="563"/>
        <source>Done.</source>
        <translation type="unfinished">Hotovo.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="536"/>
        <source>Loading list of the episodes...</source>
        <translation type="unfinished">Stahuji seznam dílů...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="546"/>
        <source>Getting information about the video...</source>
        <translation type="unfinished">Získávám informace o videu...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="580"/>
        <source>Downloading playlist...</source>
        <translation type="unfinished">Stahuji seznam videí...</translation>
    </message>
    <message>
        <source>Opening video...</source>
        <translation type="obsolete">Otevírám video...</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="WebEngineError.cpp" line="19"/>
        <source>Ok.</source>
        <translation type="unfinished">Ok.</translation>
    </message>
    <message>
        <location filename="WebEngineError.cpp" line="20"/>
        <source>Execution error!</source>
        <translation type="unfinished">Nastala chyba při vykonání příkazu!</translation>
    </message>
    <message>
        <location filename="WebEngineError.cpp" line="21"/>
        <source>Loading parser error!</source>
        <translation type="unfinished">Nastala chyba při načítání parseru!</translation>
    </message>
    <message>
        <location filename="WebEngineError.cpp" line="22"/>
        <source>Parser error!</source>
        <translation type="unfinished">Nastala chyba při parsování obsahu!</translation>
    </message>
    <message>
        <location filename="WebEngineError.cpp" line="26"/>
        <source>Unexpected error!</source>
        <translation type="unfinished">Nastala neočekávaná chyba!</translation>
    </message>
</context>
</TS>
