#ifndef _VIDEO_INFO_H_
#define _VIDEO_INFO_H_

#include <QString>
#include <QDate>

struct VideoInfo
{
	QString id;
	QString title;
	QString img;
	QString desc;
	QDate date;
	int watched;
};

#endif	// _VIDEO_INFO_H_
