#include "StreamBuffer.h"


StreamBuffer::StreamBuffer(QObject *parent) : QTemporaryFile(parent)
{
	//
}


StreamBuffer::~StreamBuffer()
{
	//
}

bool
StreamBuffer::open()
{
    setAutoRemove(false);
    return QTemporaryFile::open(QIODevice::WriteOnly);
}


qint64 StreamBuffer::appendBuffer(const char *data, qint64 len)
{
    qint64 _pos = pos();


    m_mutex.lock();
    m_rwLock.lockForRead();

    seek(size());

    qint64 _out = write(data, len);

    flush();

    seek(_pos);

    m_rwLock.unlock();
    m_mutex.unlock();

    return _out;
}

bool StreamBuffer::isSequential () const
{
    return true;

}
