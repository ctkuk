#include "bufferguard.h"
#include <QDebug>
/*======================== Constructors / destructor =============================*/
BufferGuard::BufferGuard()
{
    m_bufferSize = 0;
    m_bufferSize = -1;
    m_timeValue = 0;
    m_bufferValue = 0;
}

BufferGuard::~BufferGuard()
{

}

/*======================== Public functions =============================*/
qint64 BufferGuard::getTotalTime() const
{
    return m_totalTime;
}

qint64 BufferGuard::getBufferSize() const
{
    return m_bufferSize;
}
int BufferGuard::getPreloadTime() const
{
    return m_preloadTime;
}

bool BufferGuard::isReady() const
{
    return isValidTime(m_timeValue);
}

qint64 BufferGuard::alignValue(qint64 value) const
{
    if (isValidTime(value))
        return value;

    return qMax( qint64(0),qint64(((double(m_bufferValue)/double(m_bufferSize)) * double(m_totalTime))));

}

/*======================== Public slots =============================*/
void BufferGuard::setTotalTime(qint64 time)
{
	if(time == 0) return;
	//
	m_totalTime = time;
    // recount bytes per one milisecond
    recoutTimeRate();

}

void BufferGuard::setBufferSize(qint64 size)
{
	if(size == 0) return;
	//
    m_bufferSize = size;
    // recount bytes per one milisecond
    recoutTimeRate();
}

void BufferGuard::setPreloadTime(int time)
{
    m_preloadTime = time*1000;
}

void BufferGuard::timeTick(qint64 value)
{
    m_timeValue = value;
    // if remaining time to the end of the buffe is smaller than configured value, emit signal..
    if (((m_bufferValue - m_timeValue*m_timeRate) < m_preloadTime*m_timeRate) && m_bufferValue < m_bufferSize )
        emit preloadExceeded();
}

void BufferGuard::bufferTick(qint64 value)
{
    m_bufferValue = value;
}

/*======================== Private functions =============================*/
int BufferGuard::recoutTimeRate()
{
    if (m_totalTime)
        m_timeRate = m_bufferSize / m_totalTime;
    else
        m_timeRate = 0;
    return m_timeRate;
}

bool BufferGuard::isValidTime(qint64 value) const
{
    return ((m_bufferValue - value*m_timeRate) > m_preloadTime*m_timeRate) || m_bufferSize == m_bufferValue;
}
