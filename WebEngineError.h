#ifndef _WEB_ENGINE_ERROR_H_
#define _WEB_ENGINE_ERROR_H_

#include <QString>

enum
{
	ERR_NO,
	ERR_REQUEST,
	ERR_PARSER,
	ERR_PARSING
};

class WebEngineError
{
	private:
		int m_code;
		QString m_desc;
		
	protected:
		void getErrDesc(int code);
		
	public:
		WebEngineError(int code = 0);
		//
		inline int getErrCode() const { return m_code; }
		inline const QString & getErrDesc() const { return m_desc; }
};

#endif	// _WEB_ENGINE_ERROR_H_
