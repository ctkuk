(:~
: Funkce pro naparsovani seznamu dilu poradu
:
: @return Vystup je v nasledujicim formatu (kazda informace na vlastnim radku):
: datum odvisilani videa
: adresa obrazku videa
: popis videa
: pocet zobrazeni videa
:
: Priklad:
: 210522161600007
: 23. 2. 2010
: http://img8.ceskatelevize.cz/ivysilani/programmes/photos/w67/10090925908.jpg
: WC-klima — Twin Pigs – originalni tricka — Jidelni pribor pro jednu ruku — Tibetska medicina — Myntry.cz – stahovani z RapidSharu
: 46028
:
: Pokud neni nejaka z informaci dostupna, vlozi se pouze prazdny radek, takze pocet radku na polozku
: je fixni (3), tudiz je mozne vlozit vice polozek za sebou bez nejakeho oddelovace. Pricemz je mozne polozky
: od sebe oddelit prazdnym radkem, aby byl vypis prehlednejsi. Prazdne radky mezi polozkami jsou pri
: parsovani ignorovany, takze jich muze byt libovolny pocet.
:)

declare default element namespace "http://www.w3.org/1999/xhtml";

for $episodes in //div
where $episodes[@id='programmeMoreBox']
return
	for $item in $episodes//ul//li
		let $id      := tokenize(normalize-space(data($item//a[@class='itemSetPaging']/@href)), "/")[4]
		let $date    := normalize-space(data($item//img/@alt))
		let $img     := data($item//img/@src)
		let $desc    := normalize-space(data($item//div[@class='itemDetail']/p[1]/text()))
		let $watched := data($item//p[@class='itemInfoView']/strong/text())
		return concat("
", $id, "
", $date, "
", $img, "
", $desc, "
", $watched, "
")
