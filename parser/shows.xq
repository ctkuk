(:~
: Funkce pro naparsovani seznamu poradu
:
: @return Vystup je seznam poradu, kazdy na 2 radky:
: id poradu
: nazev poradu
:
: Priklad:
: 10151523196
: Atletika
: 
:
: Pocet radku na polozku je fixni (2), tudiz je mozne vlozit vice polozek za sebou bez nejakeho
: oddelovace. Pricemz je mozne polozky od sebe oddelit prazdnym radkem, aby byl vypis prehlednejsi.
: Prazdne radky mezi polozkami jsou pri parsovani ignorovany, takze jich muze byt libovolny pocet.
:)

declare default element namespace "http://www.w3.org/1999/xhtml";

for $shows in //div
where $shows[@id='programmeAlphabetContent']
return
	for $a in $shows//ul//a
		let $id      := tokenize(data($a/@href), "/")[3]
		let $title   := normalize-space(data($a))
		return concat("
", $id, "
", $title, "
")
