(:~
: Funkce pro naparsovani odkazu na playlist, ktery obsahuje adresu na videostream
:
: @return Vystupem je pouze 1 radek - odkaz na playlist:
: odkaz_na_playlist
:
: Priklad:
: http://89.221.209.9/iVysilani/Services/Streaming/ClientPlaylist.aspx?id=NTAwOTc1MDk1fDYzNDAyNTk3MTAyOTg0Mzc1MA==
: 
:)

declare default element namespace "http://www.w3.org/1999/xhtml";

for $divs in //div
where $divs[@id='programmePlayer']
return
	let $script := data($divs//script[last()])
	let $temp   := tokenize($script, "(flashvars\.playlistURL\ =\ )")[2]
		return tokenize($temp, '"')[2]
