(:~
: Funkce pro naparsovani poctu stranek v seznamu dilu
:
: @return Vystupem je cislo dalsi stranky ve strankovani dilu poradu.
:
: Priklad:
: 4
:
:)

declare default element namespace "http://www.w3.org/1999/xhtml";

data(//div[@id='programmeArray']//div[@class='pagingContent clearfix']//table//td[3]//a[1]/@title)
