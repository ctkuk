(:~
: Funkce pro naparsovani abecedniho seznamu
:
: @return Vystup je seznam znaku, podle nichz lze filtrovat seznam poradu.
: Kazdy je na dvou radcich:
: id znaku
: znak
:
: Priklad:
: a
: A
: %C5%BE
: Ž
: !
: #
: abeceda-vse
: vše
: 
:
: Pocet radku na polozku je fixni (2), tudiz je mozne vlozit vice polozek za sebou bez nejakeho
: oddelovace. Pricemz je mozne polozky od sebe oddelit prazdnym radkem, aby byl vypis prehlednejsi.
: Prazdne radky mezi polozkami jsou pri parsovani ignorovany, takze jich muze byt libovolny pocet.
:)

declare default element namespace "http://www.w3.org/1999/xhtml";

for $alpha in //ul
where $alpha[@id='programmeAlphabet']
return
	for $a in $alpha//li//a
		let $id      := tokenize(data($a/@href), "/")[3]
		let $title   := data($a)
		return concat("
", $id, "
", $title, "
")
