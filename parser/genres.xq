(:~
: Funkce pro naparsovani seznamu zanru
:
: @return Vystup je seznam zanru, kde je kazdy na dvou radcich:
: id zanru
: nazev zanru
:
: Priklad:
: deti
: Děti a mládež
: abeceda-vse
: vše
:
: Pocet radku na polozku je fixni (2), tudiz je mozne vlozit vice polozek za sebou bez nejakeho
: oddelovace. Pricemz je mozne polozky od sebe oddelit prazdnym radkem, aby byl vypis prehlednejsi.
: Prazdne radky mezi polozkami jsou pri parsovani ignorovany, takze jich muze byt libovolny pocet.
:)

declare default element namespace "http://www.w3.org/1999/xhtml";

for $genres in //ul
where $genres[@id='programmeGenre']
return
	for $a in $genres//li//a
		let $id      := tokenize(data($a/@href), "/")[3]
		let $title   := data($a)
		return concat("
", $id, "
", $title, "
")
