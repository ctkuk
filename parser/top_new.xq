(:~
: Funkce pro naparsovani informaci o videu
:
: @param $elems Seznam DIVu, ohranicujici jednotliva videa.
: @return Vystup je v nasledujicim formatu (kazda informace na vlastnim radku):
: id videa
: nazev videa
: adresa obrazku videa
: popis videa
: datum odvisilani videa
: pocet zobrazeni videa
:
: Priklad:
: 210562260120004
: Na cestě
: http://img4.ceskatelevize.cz/ivysilani/episodes/photos/w92/1185966822/31754.jpg
: Vojvodina
: 22. 2. 2010
: 639
:
: Pokud neni nejaka z informaci dostupna, vlozi se pouze prazdny radek, takze pocet radku na polozku
: je fixni, tudiz je mozne vlozit vice polozek za sebou bez nejakeho oddelovace. Pricemz je mozne polozky
: od sebe oddelit prazdnym radkem, aby byl vypis prehlednejsi. Prazdne radky mezi polozkami jsou pri
: parsovani ignorovany, takze jich muze byt libovolny pocet.
:)

declare default element namespace "http://www.w3.org/1999/xhtml";

(:
declare function local:get-videos( $elems as element(div)* ) as xs:string
{
:)

for $tops in //div
where $tops[@id='topNew'] or $tops[@id='topShow']
return
	for $item in $tops//div[@class='item clearfix']
		let $a       := $item//a[@class='hrefRed']
		let $id      := tokenize(normalize-space(data($a/@href)), "/")[3]
		let $title   := normalize-space(data($a))
		let $img     := normalize-space(data($item//img/@src))
		let $__desc  := data($item//p[2])
		let $_desc   := tokenize($__desc, ":")
		let $date    := normalize-space($_desc[1])
		let $desc    := normalize-space(substring($__desc, string-length($_desc[1]) + 2))
		let $_times  := tokenize(normalize-space(data($item//p[@class='smaller'])), " ")[2]
		let $watched := substring($_times, 0, string-length($_times) - 1)
		return concat("
", $id, "
", $title, "
", $img, "
", $desc, "
", $date, "
", $watched, "
")

(:
		return concat("\n", $id, "\n", $title, "\n", $img, "\n", $desc, "\n", $date, "\n", $watched, "\n")
};
:)
