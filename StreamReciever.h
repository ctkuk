#ifndef _STREAM_RECIEVER_H_
#define _STREAM_RECIEVER_H_

#include <QThread>
#include <QString>
#include <QBuffer>
#include <QReadWriteLock>

#include "StreamBuffer.h"

extern "C"
{
	#include "librtmp/rtmp.h"
}

class StreamReciever : public QThread
{
    Q_OBJECT
    
    public:
        StreamReciever(const QString &streamUrl);
        ~StreamReciever();

        void run();

		qint64 getTotalPlayTime() const;
        int getBufferSize() const;
        QString getTempFileName() const;

	signals:
		void percentage(int value);
        void loaded(qint64 value);
		void bufferReady(const QString &file);
        void bufferReady(StreamBuffer *buffer);
        void error(QString errmsg);

    private:
        QString m_url;
        QString m_tempFileName;
        StreamBuffer *m_buffer;
        QMutex m_mutex;
        QReadWriteLock m_rwLock;

        int m_bufferSize;
        qint64 m_totalTime;

};

#endif	// _STREAM_RECIEVER_H_
