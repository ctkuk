#include "volumewidget.h"

#include <QPainter>
#include <QPaintEvent>
#include <QHBoxLayout>
#include <QStyle>
#include <QDebug>

VolumeWidget::VolumeWidget(QWidget *parent) :
    QWidget(parent)
{
    setFixedWidth(105);
    setUi();

}


void VolumeWidget::setUi()
{
    m_MuteButton.setFlat(true);
    m_MuteButton.setFixedSize(26,26);
    m_MuteButton.setIconSize(QSize(24,24));
    m_MuteButton.setIcon(QPixmap(":/images/unmuted.png"));
    connect(&m_MuteButton,SIGNAL(clicked()),this,SLOT(mutedPushed()));

    m_VolumeSlider.setEnabled(true);
    m_VolumeSlider.setMinimum(0);
    m_VolumeSlider.setMaximum(100);

    m_VolumeSlider.setValue(100);

    connect(&m_VolumeSlider,SIGNAL(valueChanged(qint64)),this,SLOT(volumeChanged(qint64)));


    QHBoxLayout *_mainLayout = new QHBoxLayout(this);
    _mainLayout->setContentsMargins(1, 0, 1, 0);
    _mainLayout->addWidget(&m_MuteButton);
    _mainLayout->addWidget(&m_VolumeSlider);
    setLayout(_mainLayout);


}

void VolumeWidget::mutedPushed()
{
    if (m_pAudioOutput->isMuted())
    {
        m_pAudioOutput->setMuted(false);
        m_MuteButton.setIcon(QPixmap(":/images/unmuted.png"));
    }
    else
    {
        m_pAudioOutput->setMuted(true);
        m_MuteButton.setIcon(QPixmap(":/images/muted.png"));
    }

}

void VolumeWidget::volumeChanged(qint64 value)
{
    emit valueChanged(qreal(value)/qreal(100));
}

void VolumeWidget::setAudioOutput ( Phonon::AudioOutput * audioOutput )
{
    m_pAudioOutput = audioOutput;

    connect(this,SIGNAL(valueChanged(qreal)),m_pAudioOutput,SLOT(setVolume(qreal)));
}




/*========================================================*/
/*===============   VolumeSlider  Class   =================*/
/*========================================================*/

VolumeWidget::VolumeSlider::VolumeSlider( QWidget* parent ): Slider(parent)
{

}

VolumeWidget::VolumeSlider::~VolumeSlider()
{

}


void VolumeWidget::VolumeSlider::paintCustomSlider( QPainter *p, int x, int y, int width, int height)
{
  //  QSlider::paintEvent(new QPaintEvent(QRect(x,y,width,height)));
    const int BARS = 10;


    int _barWidth = (width) / ((2 * BARS) - 1) ;

    QLinearGradient linearGrad(QPointF(width, 0), QPointF(width, height));
    linearGrad.setColorAt(0, Qt::red);
    linearGrad.setColorAt(1, Qt::white);
    p->setBrush(linearGrad);

    bool _inverted = false;
    for (int i = 0 ; i < BARS  ; ++i)
    {
        if (!_inverted &&
            (qreal(value()) / (qreal(maximum() - qreal(minimum()))) <
             qreal(i) / qreal(BARS+1) ))
        {
            linearGrad.setColorAt(0, Qt::gray);
            p->setBrush(linearGrad);
            _inverted = false;
        }
        p->drawRect(2*i*_barWidth , BARS - i , _barWidth +1  , height - 2 - BARS + i);
    }
}
