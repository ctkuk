/*!
 * \file
 * mainwindow.h
 *
 * \author
 * Aleš Podskalský (<a href="mailto:podskale@fel.cvut.cz">podksale@fel.cvut.cz</a>)
 *
 * \brief
 * Základní okno programu ČT KuK.
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui>

#include <phonon/audiooutput.h>
#include <phonon/backendcapabilities.h>
#include <phonon/effect.h>
#include <phonon/effectparameter.h>
#include <phonon/effectwidget.h>
#include <phonon/mediaobject.h>
#include <phonon/videowidget.h>


#include "timebufferedslider.h"
#include "volumewidget.h"
#include "WebEngine.h"
#include "LinkInfo.h"
#include "StreamReciever.h"
#include "StreamBuffer.h"
#include "bufferguard.h"
#include "dialogs/about/aboutdialog.h"

class QPushButton;
class QLabel;
class QSlider;
class QTextEdit;
class QMenu;

/*!
 * \brief
 * Třída základního okna programu. Slouží pro
 * zobrazení jednotlivých komponent a spojuje
 * samostatné části programu.
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

    public:
        MainWindow(QWidget *parent = 0);
        ~MainWindow();

    private:

        /*!
         * \brief
         * Funkce pro vytvoření menu aplikace. Zde se přidávají jednotlivé položky a přiřazují jim akce.
         */
        void createMenu();
        /*!
         * \brief
         * Nastavuje a vytváří prvky GUI.
         */
        void buildGUI();

        /*!
         * \brief
         * Spojení signálů a slotů, které mají být nastaveny při startu aplikace.
         */
        void activateConnects();

        /*!
         *  \brief
         *
         */
        void afterStart();

        /*!
         *  \brief
         *
         */
        void finishUpdate();

	
    public slots:

        void openVideo(QByteArray *data);
        void openFile(const QString &file);
        void playPause();
        void stop();
        void rewind();
        void forward();

        void destroyStreamReciever();

        void stateChanged(Phonon::State newstate, Phonon::State oldstate);
        void bufferExceeded();
        void bufferReadyToPlay();

        void StreamRecieverError(QString error);
        void seekVideo(qint64 position);

        void updateTime();
        void bufferStatus(int percent);


        void addListArchiveItems(QVector<LinkInfo *> *videos);
        void showSelected(QTreeWidgetItem *item, int column);
        void episodeSelected(QTreeWidgetItem *item, int column);
        void addListEpisodeItem(VideoInfo *video);
        void allEpisodeItemsReady();
        void webEngineError(WebEngineError *err);
        void getVideoPlaylist(QString *url);


        virtual void setFullView();
        virtual void setPlayerView();
        virtual void setMinimalView();

#if defined(WIN32) && defined(RELEASE)
        void checkForUpdates();
#endif	// defined(WIN32) && defined(RELEASE)





    private:

		// GUI Widgets
        QFrame *m_pFrame;  // main frame for the mainwindow
        QStatusBar *m_pStatusBar;
        QLabel *m_pInfo;

        QTreeWidget *m_pListArchive;
        QTreeWidget *m_pListEpisodes;
        

        QWidget *m_pButtonPanelWidget;
        TimeBufferedSlider *m_pTimeSlider;
        VolumeWidget *m_pVolumeSlider;

        QPushButton *m_pRewindButton;
        QPushButton *m_pForwardButton;
        QPushButton *m_pPlayButton;
        QPushButton *m_pStopButton;

        QIcon playIcon;
        QIcon pauseIcon;
        QMenu *fileMenu;

        QPixmap *infoPicture;


        QLabel *timeLabel;
        QLabel *progressLabel;

         // polozky status baru
        QLabel *labelIcon;
        QLabel *labelText;
        QMovie *movieLoading;
        QPixmap *pixmapOk;
        QPixmap *pixmapErr;
        QPixmap *pixmapPlay;
        QPixmap *pixmapPause;
        QPixmap *pixmapStop;


        QDialog *settingsDialog;

        QWidget m_videoWindow;
        Phonon::MediaObject m_MediaObject;
        Phonon::AudioOutput *m_pAudioOutput;
        Phonon::VideoWidget *m_videoWidget;
        Phonon::Path m_audioOutputPath;
        Phonon::Path m_videoWidgetPath;

         
        WebEngine *m_webengine;
        StreamReciever *m_pStreamReciever;
        BufferGuard m_bufferGuard;

        // Actions
        QAction *saveAction;
        QAction *exitAction;

        QAction *playPauseAction;
        QAction *stopAction;

        QAction *fullViewAction;
        QAction *playerViewAction;
        QAction *minimalViewAction;

        QAction *aboutAction;
        QAction *updateAction;
        // Dialogs
    private:
        AboutDialog m_aboutDialog;
};

#endif // MAINWINDOW_H
