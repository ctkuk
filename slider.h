/*!
 * \file
 * slider.h
 *
 * \author
 * Aleš Podskalský (<a href="mailto:podskale@fel.cvut.cz">podksale@fel.cvut.cz</a>)
 *
 * \brief
 * Komponenta pro zobrazeni prubehu prehravani.
 */

#ifndef SLIDER_H
#define SLIDER_H

#include <QSlider>
#include <QPixmap>

 #include <phonon/mediaobject.h>

/*!
 * \brief
 * Trida komponenty Slideru, ktery zobrazuje mimo
 * ukazatele prehravani take zaplneni bufferu
 */
class Slider : public QSlider
{
    Q_OBJECT
    
    public:
        explicit Slider( QWidget* parent = 0 );
        virtual ~Slider();

        qint64 value() const;
        qint64 maximum() const;
        qint64 minimum() const;

        void setMinimum(qint64 min);
        void setMaximum(qint64 max);

    public slots:
        virtual void setValue( qint64 newValue );


    signals:
        //we emit this when the user has specifically changed the slider
        //so connect to it if valueChanged() is too generic
        //Qt also emits valueChanged( int )
        void sliderReleased( qint64 );
        void valueChanged( qint64 );
        
    protected:
        /*!
         * \brief
         * Fce obsluhujici udalost kolecka mysi
         */
        virtual void wheelEvent( QWheelEvent* );

        /*!
         * \brief
         * Fce oblsuhujici udalost pohybu mysi.
         */
        virtual void mouseMoveEvent( QMouseEvent* );

        /*!
         * \brief
         * Fce oblsuhujici udalost opusteni komponenty mysii.
         */
        virtual void mouseReleaseEvent( QMouseEvent* );

        /*!
         * \brief
         * Fce oblsuhujici udalost stisku mysi.
         */
        virtual void mousePressEvent( QMouseEvent* );
        virtual void slideEvent( QMouseEvent* );
        virtual bool event ( QEvent * event );
        
        virtual void paintEvent( QPaintEvent *pe );
        virtual void paintCustomSlider( QPainter *p, int x, int y, int width, int height);

        // prepocet pro prevraceny slider - asi nebude potreba
        int adjustValue( int v ) const
        {
            int mp = ( minimum() + maximum() ) / 2;
            return orientation() == Qt::Vertical ? mp - ( v - mp ) : v;
        }

        QRect sliderHandleRect( const QRect &slider, qreal percent ) const;

    protected:

        bool m_sliding;
        qint64 m_Maximum;
        qint64 m_Minimum;
        qint64 m_Value;
        qint64  m_prevValue;
        bool m_outside;




};



#endif // SLIDER_H
