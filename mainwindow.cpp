#include "mainwindow.h"
#include "mediavideowidget.h"

#if defined(WIN32) && defined(RELEASE)
	#include <QProcess>
	#include <QStringList>
#endif	// defined(WIN32) && defined(RELEASE)


/*===============================================================================*/
/*======================== Contructors / Destructor =============================*/
/*===============================================================================*/

MainWindow::MainWindow(QWidget *parent)
{
	setWindowTitle(tr("CT KuK"));
    #ifdef WIN32
    setWindowIcon(QIcon(":/images/icon.ico"));  // low resolution
    #else
    setWindowIcon(QIcon(":/images/icon-big.png")); //  nice pleasure from hi resolution only for big boys
    #endif	// WIN32

	setContextMenuPolicy(Qt::CustomContextMenu);

    // init GUI section
	createMenu();
    buildGUI();

    //
    m_pStreamReciever = 0;

    afterStart();
}


MainWindow::~MainWindow()
{
	//
    stop();
}


/*===============================================================================*/
/*=========================== Private functions =================================*/
/*===============================================================================*/

void MainWindow::createMenu()
{
    // File menu section
    QMenu *fileMenu = menuBar()->addMenu(tr("&File"));

    saveAction = fileMenu->addAction(tr("&Save buffer.."));
    saveAction->setShortcuts(QKeySequence::SaveAs);
    exitAction = fileMenu->addAction(tr("E&xit"));
    exitAction->setShortcuts(QKeySequence::Quit);

    // Playing menu section
    QMenu *playingMenu = menuBar()->addMenu(tr("&Playing"));
    playPauseAction = playingMenu->addAction(tr("&Play/Pause"));
    stopAction = playingMenu->addAction(tr("&Stop"));

    // View menu section
    QMenu *viewMenu = menuBar()->addMenu(tr("&View"));

    fullViewAction = viewMenu->addAction(tr("&Full view"));
    fullViewAction->setCheckable(true);
    fullViewAction->setChecked(true);   // default value

    playerViewAction = viewMenu->addAction(tr("&Player view"));
    playerViewAction->setCheckable(true);    

    minimalViewAction = viewMenu->addAction(tr("&Minimal view"));
    minimalViewAction->setCheckable(true);


    // View group
    QActionGroup *viewGroup = new QActionGroup(this);
    viewGroup->addAction(fullViewAction);
    viewGroup->addAction(playerViewAction);
    viewGroup->addAction(minimalViewAction);


    QMenu *helpMenu = menuBar()->addMenu(tr("&Help"));
    aboutAction = helpMenu->addAction(tr("&About"));

    connect(fullViewAction,SIGNAL(triggered()),this,SLOT(setFullView()));
    connect(playerViewAction,SIGNAL(triggered()),this,SLOT(setPlayerView()));
    connect(minimalViewAction,SIGNAL(triggered()),this,SLOT(setMinimalView()));

    connect(exitAction, SIGNAL(triggered()), qApp, SLOT(quit()));
    connect(aboutAction,SIGNAL(triggered()),&m_aboutDialog,SLOT(show()));
    
#if defined(WIN32) && defined(RELEASE)
    updateAction = helpMenu->addAction(tr("&Check for updates"));
    connect(updateAction,SIGNAL(triggered()), this,SLOT(checkForUpdates()));
#endif	// defined(WIN32) && defined(RELEASE)
}

void MainWindow::buildGUI()
{
    m_pFrame = new QFrame(this);

    QGridLayout *mainLayout = new QGridLayout(m_pFrame);
    mainLayout->setContentsMargins(0, 0, 0, 0);

    m_pStatusBar = new QStatusBar(this);

    this->resize(minimumSizeHint());
    this->setCentralWidget(m_pFrame);
    this->setStatusBar(m_pStatusBar);

    // Fake video panel setup (shows splash picture)
    m_pInfo = new QLabel(this);
    m_pInfo->setMinimumHeight(200);
    m_pInfo->setMinimumWidth(308);
    //info->setFixedSize(308,200);

    m_pInfo->setPixmap(QPixmap(":/images/background.png").scaledToHeight(m_pInfo->height()));

    m_pInfo->setAcceptDrops(false);
    m_pInfo->setMargin(2);
    m_pInfo->setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);
    m_pInfo->setLineWidth(2);
    m_pInfo->setAutoFillBackground(true);
    m_pInfo->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    QPalette palette;
    palette.setBrush(QPalette::WindowText, Qt::white);
    m_pInfo->setPalette(palette);
    mainLayout->addWidget(m_pInfo);

    // Phonon settings
    m_pAudioOutput = new Phonon::AudioOutput(Phonon::VideoCategory, this);
    m_audioOutputPath = Phonon::createPath(&m_MediaObject, m_pAudioOutput);
    m_videoWidget = new MediaVideoWidget(this);
    m_videoWidget->setContextMenuPolicy(Qt::CustomContextMenu);
    m_videoWidgetPath = Phonon::createPath(&m_MediaObject, m_videoWidget);

    QVBoxLayout *videoLayout = new QVBoxLayout(&m_videoWindow);
    videoLayout->addWidget(m_videoWidget);
    videoLayout->setContentsMargins(0, 0, 0, 0);
    m_videoWindow.setLayout(videoLayout);
    m_videoWindow.setMinimumSize(100,200);
    m_videoWindow.hide();
    m_videoWindow.setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    mainLayout->addWidget(&m_videoWindow);



    m_pButtonPanelWidget = new QWidget(this);
    m_pButtonPanelWidget->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);

    QVBoxLayout *controlPanelLayout = new QVBoxLayout(m_pButtonPanelWidget);
    controlPanelLayout->setContentsMargins(0,0,0,0);
    m_pButtonPanelWidget->setLayout(controlPanelLayout);

    mainLayout->addWidget(m_pButtonPanelWidget);

    // Control panel setup
    QHBoxLayout *_pButtonPanelLayout = new QHBoxLayout();
    _pButtonPanelLayout->setContentsMargins(0,0,0,0);

    m_pPlayButton = new QPushButton(this);

    playIcon = QPixmap(":/images/play.png");

    pauseIcon = QPixmap(":/images/pause.png");
    m_pPlayButton->setIcon(playIcon);
    m_pPlayButton->setFlat(true);
    m_pPlayButton->setFixedSize(35,35);
    m_pPlayButton->setIconSize(QSize(32,32));
    m_pPlayButton->setEnabled(false);

    m_pStopButton = new QPushButton(this);
    m_pStopButton->setIcon(QPixmap(":/images/stop.png"));
    m_pStopButton->setFlat(true);
    m_pStopButton->setFixedSize(35,35);
    m_pStopButton->setIconSize(QSize(32,32));
    m_pStopButton->setEnabled(false);

    m_pRewindButton = new QPushButton(this);
    m_pRewindButton->setIcon(QPixmap(":/images/prev.png"));
    //m_pRewindButton->setFlat(true);
    m_pRewindButton->setEnabled(false);
    m_pForwardButton = new QPushButton(this);
    m_pForwardButton->setIcon(QPixmap(":/images/next.png"));
    //m_pForwardButton->setFlat(true);
    m_pForwardButton->setEnabled(false);

    // schovat tlacitka zpet a dalsi dokud nebudou neco delat..
    m_pRewindButton->hide();
    m_pForwardButton->hide();



    _pButtonPanelLayout->addWidget(m_pRewindButton);
    _pButtonPanelLayout->addWidget(m_pPlayButton);
    _pButtonPanelLayout->addWidget(m_pStopButton);
    _pButtonPanelLayout->addWidget(m_pForwardButton);
    _pButtonPanelLayout->addStretch();


    m_pVolumeSlider = new VolumeWidget(this);
    m_pVolumeSlider->setAudioOutput(m_pAudioOutput);
    _pButtonPanelLayout->addWidget(m_pVolumeSlider);

    controlPanelLayout->addLayout(_pButtonPanelLayout);

    m_pTimeSlider = new TimeBufferedSlider(this);
    m_pTimeSlider->setMinimum(0);
    m_pTimeSlider->setMaximum(100);
    m_pTimeSlider->setMinimumBuffer(0);
    m_pTimeSlider->setMaximumBuffer(100);
    //m_pTimeSlider->setValue(40);
    //m_pTimeSlider->setBufferValue(60);
    m_pTimeSlider->setEnabled(false);
    //controlPanelLayout->addWidget(m_pTimeSlider);
    m_pTimeSlider->setMediaObject(&m_MediaObject);

    timeLabel = new QLabel(this);
    //timeLabel->setText("0/0");
    progressLabel = new QLabel(this);
    QWidget *sliderPanel = new QWidget(this);
    QHBoxLayout *sliderLayout = new QHBoxLayout(sliderPanel);
    sliderLayout->setContentsMargins(0, 0, 0, 0);
    sliderLayout->addWidget(m_pTimeSlider);
    sliderLayout->addWidget(timeLabel);
    sliderLayout->addWidget(progressLabel);
    sliderPanel->setLayout(sliderLayout);
    controlPanelLayout->addWidget(sliderPanel);


    // Lists setup
    m_pListArchive = new QTreeWidget(this);
    m_pListArchive->setHeaderLabel(tr("Programme: "));
    m_pListArchive->setRootIsDecorated(false);
    //
    m_pListEpisodes = new QTreeWidget(this);
    m_pListEpisodes->setHeaderLabel(tr("Episode: "));
    m_pListEpisodes->setRootIsDecorated(false);
    //
    mainLayout->addWidget(m_pListArchive,0,1,5,1);
    mainLayout->addWidget(m_pListEpisodes,4,0);


    labelIcon = new QLabel;
    labelIcon->setFixedSize(16,16);
    movieLoading = new QMovie(":/images/loading.gif");
    labelIcon->setMovie(movieLoading);
    this->statusBar()->addWidget(labelIcon);
    movieLoading->start();

    pixmapOk = new QPixmap(QPixmap(":/images/ok.png").scaledToHeight(16,Qt::SmoothTransformation));
    pixmapErr = new QPixmap(QPixmap(":/images/err.png").scaledToHeight(16,Qt::SmoothTransformation));
    pixmapPlay = new QPixmap(QPixmap(":/images/play.png").scaledToHeight(16,Qt::SmoothTransformation));
    pixmapPause = new QPixmap(QPixmap(":/images/pause.png").scaledToHeight(16,Qt::SmoothTransformation));
    pixmapStop = new QPixmap(QPixmap(":/images/stop.png").scaledToHeight(16,Qt::SmoothTransformation));
    
    labelText = new QLabel;
    labelText->setText(tr("Loading list of the programmes..."));
    this->statusBar()->addWidget(labelText);
    //

    adjustSize();
    activateConnects();
}

void MainWindow::activateConnects()
{
    // Web Engine connects
    connect(m_pListArchive , SIGNAL(itemDoubleClicked(QTreeWidgetItem *, int)),
            this       , SLOT(showSelected(QTreeWidgetItem *, int)));
    connect(m_pListEpisodes, SIGNAL(itemDoubleClicked(QTreeWidgetItem *, int)),
            this       , SLOT(episodeSelected(QTreeWidgetItem *, int)));

    // Control connects
    connect(m_pPlayButton, SIGNAL(clicked()), this, SLOT(playPause()));
    connect(m_pStopButton, SIGNAL(clicked()), this, SLOT(stop()));
    connect(m_pRewindButton, SIGNAL(clicked()),this,SLOT(rewind()));
    connect(m_pForwardButton,SIGNAL(clicked()),this,SLOT(forward()));
    connect(&m_MediaObject, SIGNAL(stateChanged(Phonon::State, Phonon::State)),
            this, SLOT(stateChanged(Phonon::State, Phonon::State)));
    connect(&m_MediaObject, SIGNAL(totalTimeChanged(qint64)), this, SLOT(updateTime()));
    connect(&m_MediaObject, SIGNAL(tick(qint64)), this, SLOT(updateTime()));
    connect(&m_MediaObject, SIGNAL(bufferStatus(int)), this, SLOT(bufferStatus(int)));

}

void MainWindow::afterStart()
{
    #if defined(WIN32) && defined(RELEASE)
    finishUpdate();
    #endif

    // start Web engine
    m_webengine = new WebEngine(this);

    connect(m_webengine  , SIGNAL(showsByAlphaReady(QVector<LinkInfo *> *)),
            this       , SLOT(addListArchiveItems(QVector<LinkInfo *> *)));
    connect(m_webengine  , SIGNAL(showVideoReady(VideoInfo *)),
            this       , SLOT(addListEpisodeItem(VideoInfo *)));
    connect(m_webengine  , SIGNAL(showAllVideosReady()),
            this       , SLOT(allEpisodeItemsReady()));
    connect(m_webengine  , SIGNAL(videoLinkReady(QString *)),
            this       , SLOT(getVideoPlaylist(QString *)));
    connect(m_webengine  , SIGNAL(remoteFileReady(QByteArray *)),
            this       , SLOT(openVideo(QByteArray *)));
    connect(m_webengine  , SIGNAL(error(WebEngineError *)),
            this       , SLOT(webEngineError(WebEngineError *)));

    m_webengine->getShowsByAlpha("abeceda-vse");
}

void MainWindow::finishUpdate()
{
    //
    #if defined(WIN32) && defined(RELEASE)
    // finishing update
    if(QCoreApplication::arguments().size() == 2)	// ctkuk.exe SUPPOSED_VERSION_I_UPDATED_TO
    {
        QStringList argv;
        argv.push_back(QCoreApplication::applicationVersion());
        argv.push_back(QCoreApplication::arguments().at(1));
        QProcess::startDetached("updater.exe", argv);
    }
    #endif	// defined(WIN32) && defined(RELEASE)
    //
}

/*===============================================================================*/
/*=========================== Public slots =================================*/
/*===============================================================================*/

void MainWindow::openVideo(QByteArray *data)
{
    stop();
    labelIcon->setMovie(movieLoading);
    labelText->setText(tr("Opening video..."));
    //
    QString url(*data);
	int start = url.indexOf("base=\"", url.indexOf("<switchItem")) + 7;
    int end   = url.indexOf("\"", start);
    url = url.mid(start, end - start);
    //
	m_pStreamReciever = new StreamReciever(url);
    //
    connect(m_pStreamReciever, SIGNAL(percentage(int)), m_pTimeSlider, SLOT(setBufferValue(int)));
    connect(m_pStreamReciever, SIGNAL(bufferReady(const QString &)), this, SLOT(openFile(const QString &)));
    connect(m_pStreamReciever,SIGNAL(error(QString)),this,SLOT(StreamRecieverError(QString)));
    connect(m_pStreamReciever, SIGNAL(finished()),this,SLOT(destroyStreamReciever()));
    connect(m_pTimeSlider, SIGNAL(sliderReleased(qint64)), this, SLOT(seekVideo(qint64)));
    //
    m_pStreamReciever->start();
}

void MainWindow::openFile(const QString &file)
{
    if (!m_pStreamReciever)
    {
        stop();
        labelIcon->movie()->stop();
        labelIcon->setPixmap(*pixmapErr);
        labelText->setText("Getting stream error.");
        return;
    }

    if(!file.isEmpty())
    {
        m_MediaObject.clearQueue();
        m_MediaObject.setCurrentSource(Phonon::MediaSource(file));
        m_bufferGuard.setBufferSize(m_pStreamReciever->getBufferSize());
        m_bufferGuard.setPreloadTime(10);	// 10s

        //
        // TotalTime se nastavi az pri zmene stavu do LoadingState,
        // protoze pred tim neni tento parametr k dispozici
        //
        connect(&m_MediaObject,SIGNAL(tick(qint64)),&m_bufferGuard,SLOT(timeTick(qint64)));
        connect(&m_MediaObject,SIGNAL(totalTimeChanged(qint64)),&m_bufferGuard,SLOT(setTotalTime(qint64)));
        connect(m_pStreamReciever,SIGNAL(loaded(qint64)),&m_bufferGuard,SLOT(bufferTick(qint64)));
        connect(&m_bufferGuard,SIGNAL(preloadExceeded()),this,SLOT(bufferExceeded()));

        m_bufferGuard.setTotalTime(m_pStreamReciever->getTotalPlayTime());
        m_pTimeSlider->setMaximum(m_pStreamReciever->getTotalPlayTime());
        m_pTimeSlider->setValue(m_pTimeSlider->minimum());
        m_pTimeSlider->setEnabled(true);
        qApp->processEvents();
        resize(sizeHint());

        m_videoWindow.setVisible(true);
        m_pInfo->setVisible(false);

        playPause();
    }
}

void  MainWindow::playPause()
{
    if (m_MediaObject.state() == Phonon::PlayingState)
    {
        m_MediaObject.pause();
        
    }
    else
    {
        if (m_MediaObject.currentTime() == m_bufferGuard.getTotalTime())
            m_MediaObject.seek(0);
        //
        m_MediaObject.play();
        labelText->setText(tr("Loading video..."));
        labelIcon->setMovie(movieLoading);
    }
}

void  MainWindow::stop()
{
    if (m_MediaObject.state() != Phonon::StoppedState)
        m_MediaObject.stop();
	//
    m_videoWindow.setVisible(false);
    m_pInfo->setVisible(true);
    m_pTimeSlider->setEnabled(false);
    timeLabel->setText("");

    labelText->setText("");
    labelIcon->setMovie(NULL);

    disconnect(m_pTimeSlider, SIGNAL(sliderReleased(qint64)), this, SLOT(seekVideo(qint64)));
    disconnect(&m_MediaObject,SIGNAL(tick(qint64)), &m_bufferGuard, SLOT(timeTick(qint64)));
    disconnect(&m_MediaObject,SIGNAL(totalTimeChanged(qint64)), &m_bufferGuard, SLOT(setTotalTime(qint64)));
    disconnect(&m_bufferGuard,SIGNAL(preloadExceeded()), this, SLOT(bufferExceeded()));

    destroyStreamReciever();
}


void MainWindow::rewind()
{

}

void MainWindow::forward()
{

}

void MainWindow::destroyStreamReciever()
{
    if (m_pStreamReciever)
    {
        disconnect(m_pStreamReciever,SIGNAL(loaded(qint64)),&m_bufferGuard,SLOT(bufferTick(qint64)));
        disconnect(m_pStreamReciever, SIGNAL(finished()),this,SLOT(destroyStreamReciever()));
        disconnect(m_pStreamReciever, SIGNAL(percentage(int)), m_pTimeSlider, SLOT(setBufferValue(int)));
        disconnect(m_pStreamReciever, SIGNAL(bufferReady(const QString &)), this, SLOT(openFile(const QString &)));
        disconnect(m_pStreamReciever,SIGNAL(error(QString)),this,SLOT(StreamRecieverError(QString)));


        if (m_pStreamReciever->isRunning())
        {
            m_pStreamReciever->terminate();
            m_pStreamReciever->wait();
        }
        QFile::remove(m_pStreamReciever->getTempFileName());

        delete m_pStreamReciever;
        m_pStreamReciever = 0;
    }
}


void MainWindow::stateChanged(Phonon::State newstate, Phonon::State oldstate)
 {
     switch (newstate) {
         case Phonon::ErrorState:
             QMessageBox::warning(this, "Phonon Mediaplayer", m_MediaObject.errorString(), QMessageBox::Close);
             m_pStopButton->setEnabled(false);
             if (m_MediaObject.errorType() == Phonon::FatalError) {
                 m_pPlayButton->setEnabled(false);
              //   rewindButton->setEnabled(false);
             } else {
                 m_MediaObject.pause();
                 labelIcon->setPixmap(*pixmapErr);
                 labelText->setText(tr("Error!"));
             }
             break;
         case Phonon::PausedState:
             m_pStopButton->setEnabled(true);
             labelText->setText(tr("Paused"));
			 labelIcon->setPixmap(*pixmapPause);
             m_pPlayButton->setIcon(playIcon);
             if (m_MediaObject.currentSource().type() != Phonon::MediaSource::Invalid){
                 m_pPlayButton->setEnabled(true);
             } else {
                 m_pPlayButton->setEnabled(false);
             }
             break;
         case Phonon::StoppedState:
			 labelText->setText(tr("Stopped"));
			 labelIcon->setPixmap(*pixmapStop);
             m_pStopButton->setEnabled(false);
             m_pPlayButton->setIcon(playIcon);
             m_pPlayButton->setEnabled(false);
             m_pRewindButton->setEnabled(false);
             break;
         case Phonon::PlayingState:
             labelText->setText(tr("Playing..."));
			 labelIcon->setPixmap(*pixmapPlay);
             m_pStopButton->setEnabled(true);
             m_pPlayButton->setEnabled(true);
             m_pPlayButton->setIcon(pauseIcon);
             if (m_MediaObject.hasVideo())
                 m_videoWindow.show();
             break;
         case Phonon::BufferingState:
         case Phonon::LoadingState:
			 break;
     }

 }

void MainWindow::bufferExceeded()
{
    m_MediaObject.pause();
    labelIcon->setMovie(movieLoading);
    labelIcon->movie()->start();
    labelText->setText(tr("Loading buffer..."));
    disconnect(&m_bufferGuard,SIGNAL(preloadExceeded()),this,SLOT(bufferExceeded()));
    QTimer::singleShot(1000, this, SLOT(bufferReadyToPlay()));
}


void MainWindow::bufferReadyToPlay()
{
    if (m_bufferGuard.isReady())
    {
        // reconect signal and play it
        connect(&m_bufferGuard,SIGNAL(preloadExceeded()),this,SLOT(bufferExceeded()));
        m_MediaObject.play();
    }
    else
    {
        QTimer::singleShot(1000, this, SLOT(bufferReadyToPlay()));
    }
}

void MainWindow::StreamRecieverError(QString error)
{
    /*! @todo tady by stopka asi byt nemela */
    stop();
    labelIcon->setPixmap(*pixmapErr);
    labelText->setText(error);
}

void MainWindow::seekVideo(qint64 position)
{
    m_MediaObject.seek(m_bufferGuard.alignValue(position));
}


void MainWindow::updateTime()
 {
     long len = m_bufferGuard.getTotalTime();
     long pos = m_MediaObject.currentTime();
     QString timeString;
     if (pos || len)
     {
         int sec = pos/1000;
         int min = sec/60;
         int hour = min/60;
         int msec = pos;

         QTime playTime(hour%60, min%60, sec%60, msec%1000);
         sec = len / 1000;
         min = sec / 60;
         hour = min / 60;
         msec = len;

         QTime stopTime(hour%60, min%60, sec%60, msec%1000);
         QString timeFormat = "m:ss";
         if (hour > 0)
             timeFormat = "h:mm:ss";
         timeString = playTime.toString(timeFormat);
         if (len)
             timeString += " / " + stopTime.toString(timeFormat);
     }
     timeLabel->setText(timeString);
 }



void MainWindow::bufferStatus(int percent)
{
    if (percent == 0 || percent == 100)
        progressLabel->setText(QString());
    else {
        QString str = QString::fromLatin1("(%1%)").arg(percent);
        progressLabel->setText(str);
    }
}


/* ======================== Web engine slots=====================================*/


void MainWindow::addListArchiveItems(QVector<LinkInfo *> *shows)
{
	m_pListArchive->clear();
	//
    QList<QTreeWidgetItem *> listArchiveItems;
    QTreeWidgetItem *widget;
    for (int i = 0, im = shows->size(); i < im; ++i)
    {
		widget = new QTreeWidgetItem();
        widget->setText(0, shows->at(i)->title);

		widget->setData(0, Qt::UserRole, shows->at(i)->id);
		listArchiveItems.append(widget);
	}
	//
    m_pListArchive->addTopLevelItems(listArchiveItems);
    //
    labelIcon->movie()->stop();
    labelIcon->setPixmap(*pixmapOk);
    labelText->setText(tr("Done."));
    //
    this->repaint();
    m_pListArchive->repaint();
}

void MainWindow::showSelected(QTreeWidgetItem *item, int column)
{
	m_pListEpisodes->clear();
	//
	QString id = item->data(column, Qt::UserRole).toString();
	m_webengine->getShowVideos(id);
	//
	labelIcon->setMovie(movieLoading);
	labelIcon->movie()->start();
    labelText->setText(tr("Loading list of the episodes..."));
}

void MainWindow::episodeSelected(QTreeWidgetItem *item, int column)
{
	QString id = item->data(column, Qt::UserRole).toString();
	m_webengine->getVideoLink(id);
	//
	labelIcon->setMovie(movieLoading);
	labelIcon->movie()->start();
    labelText->setText(tr("Getting information about the video..."));
}

void MainWindow::addListEpisodeItem(VideoInfo *video)
{
	static QTreeWidgetItem *widget;
	widget = new QTreeWidgetItem();
	widget->setText(0, video->date.toString("d.M.yyyy") + ": " + video->desc);
	widget->setData(0, Qt::UserRole, video->id);
	m_pListEpisodes->addTopLevelItem(widget);
	delete video;
}

void MainWindow::allEpisodeItemsReady()
{
	labelIcon->movie()->stop();
    labelIcon->setPixmap(*pixmapOk);
    labelText->setText(tr("Done."));
    //
    m_pListEpisodes->repaint();
    this->repaint();
}

void MainWindow::webEngineError(WebEngineError *err)
{
	//labelIcon->movie()->stop();
    labelIcon->setPixmap(*pixmapErr);
    labelText->setText(err->getErrDesc());
}

void MainWindow::getVideoPlaylist(QString *url)
{
	m_webengine->getRemoteFile(*url);
	//
    labelText->setText(tr("Downloading playlist..."));
}

/* ======================== Action slots=====================================*/

void MainWindow::setFullView()
{
    m_pListArchive->show();
    m_pListEpisodes->show();
    m_pButtonPanelWidget->show();

    m_pInfo->setPixmap(QPixmap(":/images/background.png").scaledToHeight(m_pInfo->height()));
    adjustSize();

}

void MainWindow::setPlayerView()
{

    m_pListArchive->hide();
    m_pListEpisodes->hide();
    m_pButtonPanelWidget->show();

    m_pInfo->setPixmap(QPixmap(":/images/background.png").scaledToHeight(m_pInfo->height()));
    resize(m_pInfo->size());
    adjustSize();

}

void MainWindow::setMinimalView()
{
    m_pListArchive->hide();
    m_pListEpisodes->hide();
    m_pButtonPanelWidget->hide();

    m_pInfo->setPixmap(QPixmap(":/images/background.png").scaledToHeight(m_pInfo->height()));
    //
    //this->setWindowFlags(Qt::WindowStaysOnTopHint);
    //this->show();
    resize(m_pInfo->size());
    adjustSize();


}



#if defined(WIN32) && defined(RELEASE)
void MainWindow::checkForUpdates()
{
	// brand new process for updater
	QProcess::startDetached("updater.exe", QStringList(QCoreApplication::applicationVersion()));
	//
	close();	// sends closeEvent
}
#endif	// defined(WIN32) && defined(RELEASE)
