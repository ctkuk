#ifndef VOLUMEWIDGET_H
#define VOLUMEWIDGET_H

#include <QWidget>
#include <QPushButton>
#include "slider.h"
#include <phonon/audiooutput.h>

class VolumeWidget : public QWidget
{
Q_OBJECT
public:
    explicit VolumeWidget(QWidget *parent = 0);

signals:
    void valueChanged(qreal);

public:
    virtual void setAudioOutput ( Phonon::AudioOutput * audioOutput );


public slots:


private slots:
    void mutedPushed();
    void volumeChanged(qint64 value);

private:
    void setUi();

private:


    class VolumeSlider : public Slider
    {

    public:
        explicit VolumeSlider( QWidget* parent = 0 );
        ~VolumeSlider();


    protected:
        /*!
         * \brief
         * Samotne vykresleni obsahu komponenty
         */
        void paintCustomSlider( QPainter *p, int x, int y, int width, int height);

    private:



    };

    VolumeSlider m_VolumeSlider;
    QPushButton m_MuteButton;

    Phonon::AudioOutput * m_pAudioOutput;
};

#endif // VOLUMEWIDGET_H
