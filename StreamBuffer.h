#ifndef _STREAM_BUFFER_H_
#define _STREAM_BUFFER_H_

#include <QBuffer>
#include <QMutex>
#include <QReadWriteLock>
#include <QTemporaryFile>

class StreamBuffer : public QTemporaryFile
{
	private:
		QMutex m_mutex;
        QReadWriteLock m_rwLock;

	protected:

	public:
		StreamBuffer(QObject *parent = 0);
		virtual ~StreamBuffer();
		
		bool open();

        qint64 appendBuffer(const char *data, qint64 len);
        bool isSequential () const ;


};

#endif	// _STREAM_BUFFER_H_
