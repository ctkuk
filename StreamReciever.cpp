#include "StreamReciever.h"

#include <QDebug>

StreamReciever::StreamReciever(const QString &streamUrl)
{
	m_url = streamUrl;
    m_buffer = new StreamBuffer();
    m_bufferSize = -1;
    m_totalTime = 0;
}

StreamReciever::~StreamReciever()
{
	delete m_buffer;
}


void
StreamReciever::run()
{
    emit(percentage(0));
	//
	const int MAX_DATA_LEN = 4096;	// 4kB
	
	//char *url = strdup(m_url.toAscii().data());
	char *url = "rtmp://prg-srv-cdn101.nacevi.cz:80/iVysilani/2011/03/168HodinCT1-130311-MP4_404p.mp4";
    //
    RTMP *stream = RTMP_Alloc();
	RTMP_Init(stream);
	RTMP_SetupURL(stream, url);
	RTMP_Connect(stream, NULL);
	RTMP_ConnectStream(stream, 0);
	//
	//free(url);
	//
    bool ready = false;
	char data[MAX_DATA_LEN];
	//

    m_buffer->open();
	if(!m_buffer->isOpen()) return;
	//
	int pos = 0, duration = RTMP_GetDuration(stream), read;
	//
	m_totalTime = stream->m_fDuration;

    m_tempFileName = m_buffer->fileName();
	int max_bitrate = 0/*stream->bitrate*/;
    const int pre_buffer_time = 15;	// 15 sec
	
	QFile file("temp.txt");
	file.open(QIODevice::OpenModeFlag::Truncate);
	file.write(m_buffer->fileName().toAscii());
	file.close();
	
	do
	{
        emit percentage(((double)stream->m_read.timestamp) / (duration * 1000.0) * 100.0);
        emit loaded(qint64(pos)-m_buffer->bytesToWrite());
		//
		read = RTMP_Read(stream, data, MAX_DATA_LEN);
		if(read > 0)
		{
			m_buffer->appendBuffer(data, read);
			pos += read;
			if(!ready)
			{
				if(stream->m_read.timestamp > pre_buffer_time)
				{
					ready = true;
					emit(bufferReady(m_buffer->fileName()));
				}
			}
		}
	} while((read > -1) && RTMP_IsConnected(stream));

    //emit loaded(qint64(len));
	RTMP_Close(stream);
	RTMP_Free(stream);
    m_buffer->close();
	//
    // emit(percentage(100));
    //
    if(!ready)
        emit(bufferReady(m_buffer->fileName()));
}

qint64 StreamReciever::getTotalPlayTime() const
{
	return m_totalTime;
}

int StreamReciever::getBufferSize() const
{
    return m_bufferSize;
}

QString StreamReciever::getTempFileName() const
{
    return m_tempFileName;
}
