#include "slider.h"

#include <QContextMenuEvent>
#include <QPainter>
#include <QStyle>

/*======================== Contructors / Destructor =============================*/

Slider::Slider( QWidget *parent ): QSlider( Qt::Horizontal, parent )
{
    setAttribute( Qt::WA_NoMousePropagation, true );
    setFocusPolicy( Qt::NoFocus );
}

Slider::~Slider()
{


}

/*======================== Public functions =============================*/

qint64 Slider::value() const
{
    return m_Value;//adjustValue( QSlider::value() );
}

qint64 Slider::maximum() const
{
    return m_Maximum;

}
qint64 Slider::minimum() const
{
    return m_Minimum;
}

void Slider::setMinimum(qint64 min)
{
    m_Minimum = min;
}

void Slider::setMaximum(qint64 max)
{
    m_Maximum = max;
}

/*======================== Public slots =============================*/

void Slider::setValue( qint64 newValue )
{
    m_Value = newValue;//
    emit valueChanged(m_Value);
    if ( !m_sliding || m_outside )
    {
        m_Value = newValue;//
        //QSlider::setValue( adjustValue( int(newValue )) );
    }
    else
        m_prevValue = newValue;// adjustValue( newValue );

    //
    this->repaint();
}

/*======================== Protocected functions =============================*/

void Slider::wheelEvent( QWheelEvent *e )
{
    if( orientation() == Qt::Vertical )
    {
        // Will be handled by the parent widget
        e->ignore();
        return;
    }

    // Position Slider (horizontal)
    // only used for progress slider now!
    int step = 10 * (e->delta() > 0 ? 1 : -1);// * 24;
    qint64 nval = m_Value + step;//adjustValue( QSlider::value() + step );
    nval = qMax(qint64(nval), minimum());
    nval = qMin(qint64(nval), maximum());

    Slider::setValue( nval );

    emit sliderReleased( value() );

}

void Slider::mouseMoveEvent( QMouseEvent *e )
{

    if ( m_sliding )
    {
        //feels better, but using set value of 20 is bad of course
        QRect rect( -20, -20, width()+40, height()+40 );

        if ( orientation() == Qt::Horizontal && !rect.contains( e->pos() ) )
        {
            if ( !m_outside )
            {
                setValue( m_prevValue );
                //if mouse released outside of slider, emit sliderMoved to previous value
                emit sliderMoved( m_prevValue );
            }
            m_outside = true;
        }
        else
        {
            m_outside = false;
            slideEvent( e );
            emit sliderMoved( value() );
        }
    }
    else
    {
        QSlider::mouseMoveEvent( e );
        emit sliderReleased( value() );
    }
}

void Slider::slideEvent( QMouseEvent *e )
{
    QRect knob;
    if ( maximum() > minimum() )
        knob = sliderHandleRect( rect(), (value()) / ( maximum() - minimum() ) );

    int position;
    int span;

    if( orientation() == Qt::Horizontal )
    {
        position = e->pos().x() - knob.width() / 2;
        span = width() - knob.width();
    }
    else
    {
        position = e->pos().y() - knob.height() / 2;
        span = height() - knob.height();
    }

    const int val = QStyle::sliderValueFromPosition( minimum(), maximum(), position, span );
    setValue( val );
}

void Slider::mousePressEvent( QMouseEvent *e )
{
    m_sliding   = true;
    m_prevValue = adjustValue( value() );

    QRect knob;
    if ( maximum() > minimum() )
        knob = sliderHandleRect( rect(), (value()) / ( maximum() - minimum() ) );
    if ( !knob.contains( e->pos() ) )
        mouseMoveEvent( e );
}

void Slider::mouseReleaseEvent( QMouseEvent* )
{
    if( !m_outside && adjustValue( value() ) != m_prevValue )
       emit sliderReleased( value() );
    emit sliderReleased( value() );
    m_sliding = false;
    m_outside = false;
}

void Slider::paintEvent( QPaintEvent *pe )
{
    QPainter p( this );
    p.setClipRegion(pe->region());
    paintCustomSlider( &p, 0, 0, width(), height());

}


bool Slider::event( QEvent * event )
{
    if( event->type() == QEvent::ToolTip )
    {
        // Make a QHelpEvent out of this
        QHelpEvent * helpEvent = dynamic_cast<QHelpEvent *>( event );
        if( helpEvent )
        {
            // Update tooltip to show the current track time position
            //const int trackPosition = The::engineController()->trackPosition();
            //setToolTip( tr( "Time position: %1", Meta::secToPrettyTime( trackPosition ) ) );
        }
    }

    return QWidget::event( event );
}

void Slider::paintCustomSlider( QPainter *p, int x, int y, int width, int height)
{
    QSlider::paintEvent(new QPaintEvent(QRect(x,y,width,height)));
}




/*======================== Private =============================*/

QRect Slider::sliderHandleRect( const QRect &slider, qreal percent ) const
{
    QRect rect;

    const int handleSize = style()->pixelMetric( QStyle::PM_SliderControlThickness );
    rect = QRect( 0, 0, handleSize, handleSize );
    rect.moveTo( slider.x() + qRound( ( slider.width() - handleSize ) * percent ), slider.y() + 1 );

    return rect;
}

