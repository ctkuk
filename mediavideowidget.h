#ifndef MEDIAVIDEOWIDGET_H
#define MEDIAVIDEOWIDGET_H

#include <QtGui>

#include <phonon/audiooutput.h>
#include <phonon/backendcapabilities.h>
#include <phonon/effect.h>
#include <phonon/effectparameter.h>
#include <phonon/effectwidget.h>
#include <phonon/mediaobject.h>
#include <phonon/seekslider.h>
#include <phonon/videowidget.h>
#include <phonon/volumeslider.h>

class MediaVideoWidget : public Phonon::VideoWidget
{
public:
    MediaVideoWidget(/*MainWindow *player,*/ QWidget *parent = 0);

protected:
    void mouseDoubleClickEvent(QMouseEvent *e);
    void keyPressEvent(QKeyEvent *e);
    bool event(QEvent *e);
    void timerEvent(QTimerEvent *e);
    void dropEvent(QDropEvent *e);
    void dragEnterEvent(QDragEnterEvent *e);

private:
    //MainWindow *m_player;
    QBasicTimer m_timer;
    QAction m_action;
};

#endif // MEDIAVIDEOWIDGET_H
