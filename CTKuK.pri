# ----------------------------------------------------
# for WINDOWS
# ------------------------------------------------------
QT += network \
    xml \
    xmlpatterns \
    phonon \
    multimedia \
    webkit \
    svg
TARGET = CTKuK
DESTDIR = ../bin
OBJECTS_DIR = ../build
MOC_DIR = ../build
UI_DIR = ../build
RCC_DIR = ../build
TEMPLATE = app
HEADERS += ./bufferguard.h \
    ./LinkInfo.h \
    ./mainwindow.h \
    ./mediavideowidget.h \
    ./ShowInfo.h \
    ./slider.h \
    ./StreamBuffer.h \
    ./StreamReciever.h \
    ./timebufferedslider.h \
    ./VideoInfo.h \
    ./volumewidget.h \
    ./WebEngine.h \
    ./WebEngineError.h \
    ./mplayer/af_format.h \
    ./mplayer/asf.h \
    ./mplayer/asf_mmst_streaming.h \
    ./mplayer/asfguid.h \
    ./mplayer/ass.h \
    ./mplayer/ass_types.h \
    ./mplayer/ass_utils.h \
    ./mplayer/avstring.h \
    ./mplayer/common.h \
    ./mplayer/config.h \
    ./mplayer/cookies.h \
    ./mplayer/demuxer.h \
    ./mplayer/help_mp.h \
    ./mplayer/http.h \
    ./mplayer/img_format.h \
    ./mplayer/intreadwrite.h \
    ./mplayer/m_option.h \
    ./mplayer/m_struct.h \
    ./mplayer/mem.h \
    ./mplayer/mp_msg.h \
    ./mplayer/network.h \
    ./mplayer/stream.h \
    ./mplayer/tcp.h \
    ./mplayer/url.h \
    ./mplayer/version.h \
    ./dialogs/about/aboutdialog.h \
    ./resource.h
SOURCES += ./bufferguard.cpp \
    ./LinkInfo.cpp \
    ./main.cpp \
    ./mainwindow.cpp \
    ./mediavideowidget.cpp \
    ./ShowInfo.cpp \
    ./slider.cpp \
    ./StreamBuffer.cpp \
    ./StreamReciever.cpp \
    ./timebufferedslider.cpp \
    ./VideoInfo.cpp \
    ./volumewidget.cpp \
    ./WebEngine.cpp \
    ./WebEngineError.cpp \
    ./mplayer/asf_mmst_streaming.c \
    ./mplayer/asf_streaming.c \
    ./mplayer/avstring.c \
    ./mplayer/cookies.c \
    ./mplayer/http.c \
    ./mplayer/m_option.c \
    ./mplayer/m_struct.c \
    ./mplayer/mp_msg.c \
    ./mplayer/network.c \
    ./mplayer/stream.c \
    ./mplayer/tcp.c \
    ./mplayer/url.c \
    ./dialogs/about/aboutdialog.cpp
RESOURCES += resources.qrc
CODECFORTR = UTF-8
CODECFORSRC = UTF-8
TRANSLATIONS = CTKuK_cs_CZ.ts
FORMS += dialogs/about/aboutdialog.ui
RC_FILE = CTKuK.rc
