#ifndef TIMEBUFFEREDSLIDER_H
#define TIMEBUFFEREDSLIDER_H

#include "slider.h"

class TimeBufferedSlider : public Slider
{
    Q_OBJECT
public:
    explicit TimeBufferedSlider( QWidget* parent = 0 );
    ~TimeBufferedSlider();

    int valueBuffer() const;
    int maximumBuffer() const;
    int minimumBuffer() const;


    virtual void setMaximumBuffer(int max);
    virtual void setMinimumBuffer(int min);
    virtual void setRangeBuffer(int min, int max);

public slots:
    virtual void setBufferValue( int value );

    virtual void setMediaObject ( Phonon::MediaObject * media );
    virtual void mediaVideoChanged(bool hasVideo);



protected:
    /*!
     * \brief
     * Samotne vykresleni obsahu komponenty
     */
    void paintCustomSlider( QPainter *p, int x, int y, int width, int height);



    int  m_bufferValue;
    int  m_bufferMinimum;
    int  m_bufferMaximum;




private:



    Phonon::MediaObject * m_pMediaObject;

};

#endif // TIMEBUFFEREDSLIDER_H
