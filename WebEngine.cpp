#include "WebEngine.h"

#include <QXmlQuery>
#include <QTextStream>
#include <QFile>

#ifdef HTMLTIDY_LOCAL
#include <tidy.h>
#endif	// HTMLTIDY_LOCAL

//////////
// TODO:
//  - parsing
//    -- refaktor parsingu ... sloucit nacitani podle jednotlivych datovych struktur
//    -- nastava chyba pri parsovani - ta je zrejme zpusobena timeoutem, protoze se vrati prazdny obsah
//       - poslat tedy ten samy request, pokud toto nastane
//  + zive vysilani
//
//
//  - co kdyz spustim 2 requesty po sobe a prvni se jeste nedokoncil? (prepise se mi reply/request)
//    -- zvazit blokovani nejakym booleanem, cimz se otevre cesta i pro vlakna - 1 request = 1 vlakno
//  - doxygen
//  - leaky? m_net, m_reply, m_request...nejsem si tim jistej...
//////////

void
WebEngine::slotNewestVideosReady(QNetworkReply *reply)	// reply == m_reply
{
	int err;
	QString result;
	if((err = parse("parser/top_new.xq", reply->readAll(), &result)) != ERR_NO)
	{
		emit error(new WebEngineError(err));
		return;
	}
	// get data
	QTextStream stream(&result, QIODevice::ReadOnly);
	QString tmp;
	QVector<VideoInfo *> *videos = new QVector<VideoInfo *>();
	VideoInfo *video;
	while(1)
	{
		tmp = stream.readLine();
		if(tmp.isNull()) break;
		//
		tmp = tmp.trimmed();
		if(tmp.length() > 0)
		{
			video = new VideoInfo;
			video->id = tmp;
			video->title = stream.readLine().trimmed();
			video->img = stream.readLine().trimmed();
			video->desc = stream.readLine().trimmed();
			video->date = QDate::fromString(stream.readLine().trimmed(), "d. M. yyyy");
			video->watched = stream.readLine().trimmed().toInt();
			videos->push_back(video);
		}
	}
	m_working = false;
	emit newestVideosReady(videos);
}

void
WebEngine::slotMostWatchedVideosReady(QNetworkReply *reply)
{
	int err;
	QString result;
	if((err = parse("parser/top_new.xq", reply->readAll(), &result)) != ERR_NO)
	{
		emit error(new WebEngineError(err));
		return;
	}
	// parsing data
	QTextStream stream(&result, QIODevice::ReadOnly);
	QString tmp;
	QVector<VideoInfo *> *videos = new QVector<VideoInfo *>();
	VideoInfo *video;
	while(1)
	{
		tmp = stream.readLine();
		if(tmp.isNull()) break;
		//
		tmp = tmp.trimmed();
		if(tmp.length() > 0)
		{
			video = new VideoInfo;
			video->id = tmp;
			video->title = stream.readLine().trimmed();
			video->img = stream.readLine().trimmed();
			video->desc = stream.readLine().trimmed();
			video->date = QDate::fromString(stream.readLine().trimmed(), "d. M. yyyy");
			video->watched = stream.readLine().trimmed().toInt();
			videos->push_back(video);
		}
	}
	m_working = false;
	emit mostWatchedVideosReady(videos);
}

void
WebEngine::slotAlphabetReady(QNetworkReply *reply)
{
	int err;
	QString result;
	if((err = parse("parser/alphabet.xq", reply->readAll(), &result)) != ERR_NO)
	{
		emit error(new WebEngineError(err));
		return;
	}
	// parsing data
	QTextStream stream(&result, QIODevice::ReadOnly);
	QString tmp;
	QVector<LinkInfo *> *genres = new QVector<LinkInfo *>();
	LinkInfo *genre = NULL;
	while(1)
	{
		tmp = stream.readLine();
		if(tmp.isNull()) break;
		//
		tmp = tmp.trimmed();
		if(tmp.length() > 0)
		{
			genre = new LinkInfo;
			genre->id = tmp;
			genre->title = stream.readLine().trimmed();
			genres->push_back(genre);
		}
	}
	m_working = false;
	emit alphabetReady(genres);
}

void
WebEngine::slotGenresReady(QNetworkReply *reply)
{
	int err;
	QString result;
	if((err = parse("parser/genres.xq", reply->readAll(), &result)) != ERR_NO)
	{
		emit error(new WebEngineError(err));
		return;
	}
	// parsing data
	QTextStream stream(&result, QIODevice::ReadOnly);
	QString tmp;
	QVector<LinkInfo *> *genres = new QVector<LinkInfo *>();
	LinkInfo *genre = NULL;
	while(1)
	{
		tmp = stream.readLine();
		if(tmp.isNull()) break;
		//
		tmp = tmp.trimmed();
		if(tmp.length() > 0)
		{
			genre = new LinkInfo;
			genre->id = tmp;
			genre->title = stream.readLine().trimmed();
			genres->push_back(genre);
		}
	}
	m_working = false;
	emit genresReady(genres);
}

void
WebEngine::slotShowsByAlphaReady(QNetworkReply *reply)
{
	int err;
	QString result;
	if((err = parse("parser/shows.xq", reply->readAll(), &result)) != ERR_NO)
	{
		emit error(new WebEngineError(err));
		return;
	}
	// parsing data
	QTextStream stream(&result, QIODevice::ReadOnly);
	QString tmp;
	QVector<LinkInfo *> *shows = new QVector<LinkInfo *>();
	LinkInfo *show = NULL;
	while(1)
	{
		tmp = stream.readLine();
		if(tmp.isNull()) break;
		//
		tmp = tmp.trimmed();
		if(tmp.length() > 0)
		{
			show = new LinkInfo;
			show->id = tmp;
			show->title = stream.readLine().trimmed();
			shows->push_back(show);
		}
	}
	m_working = false;
	emit showsByAlphaReady(shows);
}

void
WebEngine::slotShowsByGenreReady(QNetworkReply *reply)
{
	int err;
	QString result;
	if((err = parse("parser/shows.xq", reply->readAll(), &result)) != ERR_NO)
	{
		emit error(new WebEngineError(err));
		return;
	}
	// parsing data
	QTextStream stream(&result, QIODevice::ReadOnly);
	QString tmp;
	QVector<LinkInfo *> *shows = new QVector<LinkInfo *>();
	LinkInfo *show = NULL;
	while(1)
	{
		tmp = stream.readLine();
		if(tmp.isNull()) break;
		//
		tmp = tmp.trimmed();
		if(tmp.length() > 0)
		{
			show = new LinkInfo;
			show->id = tmp;
			show->title = stream.readLine().trimmed();
			shows->push_back(show);
		}
	}
	m_working = false;
	emit showsByGenreReady(shows);
}

void
WebEngine::slotVideosByDateReady(QNetworkReply *reply)
{
	m_working = false;
	emit error(new WebEngineError(-1));	// -1 == unexpected error
	//emit videosByDateReady(videos);
}

void
WebEngine::slotShowVideosReady(QNetworkReply *reply)
{
    int err;
    QString result;
    QByteArray *data = new QByteArray(reply->readAll());
    //
    if((err = parse("parser/episodes-pages.xq", *data, &result)) != ERR_NO)
    {
        emit error(new WebEngineError(err));
        return;
    }
    int nextPage = result.toInt();
    //
    m_parser.addTask(new QPair<QString, QByteArray *>("parser/episodes.xq", data));
    //
    if(nextPage)
    {
        getShowVideos(m_requestedShowId, nextPage);
    }
    else
    {
        m_requestedShowId.clear();
        m_working = false;
        emit showAllVideosReady();
    }
}

void
WebEngine::slotShowBonusVideosReady(QNetworkReply *reply)
{
	m_working = false;
	emit error(new WebEngineError(-1));	// -1 == unexpected error
	//emit showBonusVideosReady(videos); link-ivysilani/id/bonusy/1,2,3..max
}

void
WebEngine::slotVideoLinkReady(QNetworkReply *reply)
{
	int err;
	QString *result = new QString("");

    if((err = parse("parser/videolink.xq", reply->readAll(), result)) != ERR_NO)
    {
        emit error(new WebEngineError(err));
        return;
    }

	//    
    emit videoLinkReady(result);
}

void
WebEngine::slotRemoteFileReady(QNetworkReply *reply)
{
	QByteArray *data = new QByteArray(reply->readAll());
	//
	emit remoteFileReady(data);
}

void
WebEngine::slotError(QNetworkReply::NetworkError errCode)
{
	m_working = false;
	emit error(new WebEngineError(ERR_REQUEST));
}

void
WebEngine::emitError(WebEngineError *err)
{
    emit error(err);
}

void
WebEngine::emitShowVideoReady(VideoInfo *video)
{
    emit showVideoReady(video);
}

QByteArray WebEngine::userAgent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; cs; rv:1.9.2) Gecko/20100115 Firefox/3.6";
//
QString WebEngine::iVysilaniLink           = "http://www.ceskatelevize.cz/ivysilani/";
QString WebEngine::theNewestLink           = "http://www.ceskatelevize.cz/ivysilani/nejnovejsi/";
QString WebEngine::theMostWatchedLink      = "http://www.ceskatelevize.cz/ivysilani/nejsledovanejsi/";
QString WebEngine::alphabeticalySortedLink = "http://www.ceskatelevize.cz/ivysilani/podle-abecedy/";

#ifdef HTMLTIDY_LOCAL
QByteArray &
WebEngine::htmlTidy(QByteArray &arr) const
{
	TidyDoc tdoc = tidyCreate();
	tidyParseString(tdoc, arr.data());
	tidyCleanAndRepair(tdoc);
	//
	uint buflen = 0;
	char *outbuf = NULL;
	tidySaveString(tdoc, outbuf, &buflen);	// do buflen se mi vrati velikost vystupu
	//
	outbuf = new char[buflen+1];	// pro '\0'
	tidySaveString(tdoc, outbuf, &buflen);
	outbuf[buflen] = 0;
	//
	arr = outbuf;
	return arr;
}
#endif	// HTMLTIDY_LOCAL

QString
WebEngine::htmlTidyViaWeb(const QString &url) const
{
    QString retval = "http://services.w3.org/tidy/tidy?docAddr=" + url;
	return retval;
}



QByteArray &
WebEngine::fromUtf8toLocal8Bit(QByteArray &arr) const
{
    arr = QString::fromUtf8(arr.data()).toLocal8Bit();
    return arr;
}

WebEngine::WebEngine(QObject *parent) : QObject(parent)
{
	m_parent = parent;
	m_net = NULL;
	m_request = NULL;
	m_reply = NULL;
	m_working = false;
    //
    connect(&m_parser, SIGNAL(showVideoReady(VideoInfo *)), this, SLOT(emitShowVideoReady(VideoInfo *)));
    connect(&m_parser, SIGNAL(error(WebEngineError *)), this, SLOT(emitError(WebEngineError *)));
}

WebEngine::~WebEngine()
{
    if(!m_parser.isFinished())
	{
        m_parser.stop();
		this->thread()->wait(250);	// wait for 250ms
	}
	//
	while(m_parser.isFinished())
	{
		m_parser.terminate();
		this->thread()->wait(250);	// wait for 250ms
	}
    //
	if(!m_parent)
		m_net->deleteLater();	// kdyz je prirazen rodic, smaze se objekt automaticky sam!
	//
	delete m_request;
}

void
WebEngine::doRequest(const QString &url, bool tidyHtml)
{
	m_working = true;
	m_net = new QNetworkAccessManager(this);
	m_request = new QNetworkRequest;
	m_request->setRawHeader("User-Agent", userAgent);
	m_request->setUrl(QUrl(tidyHtml ? htmlTidyViaWeb(url) : url));
	m_reply = m_net->get(*m_request);
    //
    connect(m_reply, SIGNAL(error(QNetworkReply::NetworkError)),
            this   , SLOT  (slotError(QNetworkReply::NetworkError)));
}

void
WebEngine::doVideoRequest(const QString &url, bool tidyHtml)
{
    m_netVideo = new QNetworkAccessManager(this);
    m_requestVideo = new QNetworkRequest;
    m_requestVideo->setRawHeader("User-Agent", userAgent);
    m_requestVideo->setUrl(QUrl(tidyHtml ? htmlTidyViaWeb(url) : url));
    m_replyVideo = m_netVideo->get(*m_requestVideo);
    //
    connect(m_replyVideo, SIGNAL(error(QNetworkReply::NetworkError)),
            this   , SLOT  (slotError(QNetworkReply::NetworkError)));
}

int
WebEngine::parse(const QString &parserPath, const QByteArray &data, QString *result)
{
	if(result == NULL)
	{
		return ERR_PARSING;
	}
	//
	QFile parser(parserPath);
	parser.open(QIODevice::ReadOnly);
	if(!parser.isOpen())
	{
		return ERR_PARSER;
	}
	QByteArray get_videos = parser.readAll();
	parser.close();
	//
	QXmlQuery query(QXmlQuery::XQuery10);
	QByteArray _data = data;
    if(query.setFocus(fromUtf8toLocal8Bit(_data)))
	{
		query.setQuery(get_videos);
		if(query.isValid())
		{
			query.evaluateTo(result);
		}
		else
		{
			return ERR_PARSER;
		}
	}
	else
	{
		return ERR_PARSING;
	}
	return ERR_NO;
}

void
WebEngine::getNewestVideos()
{
	doRequest(theNewestLink);
	//
	connect(m_net, SIGNAL(finished(QNetworkReply *)),
			this , SLOT  (slotNewestVideosReady(QNetworkReply *)));
}

void
WebEngine::getMostWatchedVideos()
{
	doRequest(theMostWatchedLink);
	//
	connect(m_net, SIGNAL(finished(QNetworkReply *)),
			this , SLOT  (slotMostWatchedVideosReady(QNetworkReply *)));
}

void
WebEngine::getAlphabet()
{
	doRequest(alphabeticalySortedLink);
	//
	connect(m_net, SIGNAL(finished(QNetworkReply *)),
			this , SLOT  (slotAlphabetReady(QNetworkReply *)));
}

void
WebEngine::getGenres()
{
	doRequest(alphabeticalySortedLink);
	//
	connect(m_net, SIGNAL(finished(QNetworkReply *)),
			this , SLOT  (slotGenresReady(QNetworkReply *)));
}

void
WebEngine::getShowsByAlpha(const QString &c)
{
	doRequest(iVysilaniLink + c);
	//
	connect(m_net, SIGNAL(finished(QNetworkReply *)),
			this , SLOT  (slotShowsByAlphaReady(QNetworkReply *)));
}
void
WebEngine::getShowsByGenre(const QString &genre)
{
	doRequest(iVysilaniLink + genre);
	//
	connect(m_net, SIGNAL(finished(QNetworkReply *)),
			this , SLOT  (slotShowsByGenreReady(QNetworkReply *)));
}

void
WebEngine::getVideosByDate(const QDate &date)
{
	doRequest(iVysilaniLink + date.toString("d.M.yyyy"));
	//
	connect(m_net, SIGNAL(finished(QNetworkReply *)),
			this , SLOT  (slotVideosByDateReady(QNetworkReply *)));
}

void
WebEngine::getShowVideos(const QString &id, int page)
{
    if(m_requestedShowId != id) // zmena poradu?
    {
        if(!m_parser.isRunning())    // prvni vyber poradu
        {
            m_parser.start();
        }
        else
        {
            m_parser.cleanTasks();
        }
    }
    //
	m_requestedShowId = id;
	doRequest(iVysilaniLink + id + "/dalsi-casti/" + QString::number(page));
	//
	connect(m_net, SIGNAL(finished(QNetworkReply *)),
			this , SLOT  (slotShowVideosReady(QNetworkReply *)));
}

void
WebEngine::getShowBonusVideos(const QString &id)
{
	doRequest(iVysilaniLink + id);
	//
	connect(m_net, SIGNAL(finished(QNetworkReply *)),
			this , SLOT  (slotShowBonusVideosReady(QNetworkReply *)));
}

void
WebEngine::getVideoLink(const QString &id)
{
    doVideoRequest(iVysilaniLink + id);
    //
    connect(m_netVideo, SIGNAL(finished(QNetworkReply *)),
			this , SLOT  (slotVideoLinkReady(QNetworkReply *)));
}

void
WebEngine::getRemoteFile(const QString &url)
{
    doVideoRequest(url, false);
	//
    connect(m_netVideo, SIGNAL(finished(QNetworkReply *)),
			this , SLOT  (slotRemoteFileReady(QNetworkReply *)));
}

bool
WebEngine::isWorking() const
{
	return m_working;
}
